# PASHA

## Name
PASHA (PedAgogic StocHastic data Assimilation)

## Description
The global structure of the package is mainly composed of a **model** class which implements the model and a **DAscheme** class which implements the data assimilation method that is used. The model class is composed of a set of methods allowing to initialize the model, to propagate it on one or several consecutive time steps and to update the initial conditions and/or the parameters for the next assimilation cycle. The DAscheme class allows to drive the whole assimilation experiment by performing alternating prediction and analysis steps. Different variants of the Kalman filter are integrated in the form of classes inheriting from DAscheme: 

* EnKF (two integrated variants: Ensemble Transform Kalman Filter (Bishop et al. 2001) and stochastic version of EnKF as proposed in Burgers et al. 1998), 
* ES
* ES-MDA 
* iEnKS 

For each of them, the analysis and DAfull methods are redefined according to the strategy used for the analysis and according to the type of assimilation considered (filter or fixed point smoother or fixed interval smoother with fixed interval). No numerical optimization has been done and the algorithms have been kept as compact and transparent as possible, which allows a better overview of the mathematical theory they implement. 
Moreover, an **ObsInfo** class allows to provide all the information related to the observations (values, frequency, observation operator, error covariance matrix,) and a **Trajectory** class allows to extract some values from the state vector to constitute temporal trajectories. The ObsInfo and Trajectory classes also include a set of methods facilitating the visualization of the temporal evolution of the set of observations and their errors. Finally, the **PerfStudy** class allows to compute a set of of metrics allowing to evaluate the quality of the assimilation results in relation to a reference trajectory (bias, RMSE, dispersion, CRPS).

## Citation
Please cite this package as:
Rouzies, Emilie, 2024, "PASHA (PedAgogic StocHastic data Assimilation)", https://doi.org/10.57745/Y4TZ0J, Recherche Data Gouv.

## Visuals
![PASHA](/uploads/c1130ac626d78b7ffc216a1e251da9c5/PASHA.png)

## Installation
It is need to install a virtual environment (SA_venv in what follows).
```bash
    cd ~/
    python3 -m venv SA_venv
    source ~/SA_venv/bin/activate
    cd /path/to/packagePASHA
    pip install -r requirements.txt
```

Then, you need to run SA_venv to use PASHA package 
```bash
source ~/SA_venv/bin/activate
```


## Usage
The use of PASHA for a new model requires the creation of a new class which inherits from model and where the oneStep method allowing to launch the model on a time step is redefined according to the specificities of the latter. The methods for update the initial conditions and the model parameters must also be redefined. The source codes are provided with several examples of applications (test_PASHA directory) based on a Lorenz model and an anharmonic oscillator to allow a first handling. 

## License
For open source projects, say how it is licensed.

