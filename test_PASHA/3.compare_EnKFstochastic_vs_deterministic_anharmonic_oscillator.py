import os,sys
import numpy as np
import copy
import itertools
import matplotlib.pyplot as plt

path_PASHA = 'path/to/packagePASHA'
sys.path.append(path_PASHA)

import PASHA.models.anh_oscillator as anh
import PASHA.models.trajectory as trj
import PASHA.assimilation.assim_ens_filter as DAfilt
import PASHA.models.perf_study as perf

verbuose=2
np.random.seed(444)

#########################################
##############    SETUP    ##############
#########################################
TEND      = 1000
WW        = 3.5*1e-2 ## omega
LL        = 3*1e-4 ## lambda
x0        = 0
x1        = 1
u0        = np.array([x1,x0])
M         = 30 ### members
N         = 2 ### dimension
SIGMA     = 1 ### error on CI
R_SCAL    = 25**2 ### obs standard deviation
R         = np.array([[R_SCAL]])
OBS_STEP  = 25 ### obs. frequency
##########################################
##########################################

testOscil = anh.AnharmonicOscillator(W=WW,L=LL)
Obs       = anh.ObsInfo(OBS_STEP=OBS_STEP,R=R)

########################
### 0. Intialise members
######################## 

members_i = np.empty((N,M)) 
for i in range(M):
	members_i[:,i] = [comp + np.random.normal(scale = SIGMA) for comp in u0]
    
    
##############################
### 1. Initialise trajectories
############################## 

TrajEnKFstoch_dic   = {"position":trj.Trajectory(np.flip(members_i),[0,1],M,name="EnKFstoch",select_op=[1,0]) }
TrajEnKFdeterm_dic  = {"position":trj.Trajectory(np.flip(members_i),[0,1],M,name="EnKFdeterm",select_op=[1,0])}  
TrajOL_dic          = {"position":trj.Trajectory(np.flip(members_i),[0,1],M,name="OL",select_op=[1,0])  }
TrajTrue_dic        = {"position":trj.Trajectory(u0[::-1],[0,1],1,name="True",select_op=[1,0])  }

########################################
### 2. Initialise performance indicators
######################################## 

PerfStoch  = perf.PerfStudy(TrajEnKFstoch_dic['position'])
PerfDeterm = perf.PerfStudy(TrajEnKFdeterm_dic['position'])
PerfOL     = perf.PerfStudy(TrajOL_dic['position'])
PerfTrue   = perf.PerfStudy(TrajTrue_dic['position'])

################
### 3. Open Loop
################ 

members = members_i

for it,t in enumerate(np.arange(2,TEND+1)):
    members,TrajOL_dic = testOscil.oneStepForFullEnsemble(t,t+1,members,traj_dic=TrajOL_dic)
    
###########################    
### 4. Generate virtual obs
###########################

u  = u0 

for t in np.arange(2,TEND+1):
    
    ## Forecast (reference model)
    u, TrajTrue_dic = testOscil.oneStep(t,t+1,u,traj_dic=TrajTrue_dic)
    TrajTrue_dic['position'].append(TrajTrue_dic['position'].pick(u),t)
    
    if t % OBS_STEP == 0 : ## Observation available: perform analysis
        
        ## Get virtual observation
        obs_noisy = Obs.noisy(Obs.H(u))
        Obs.append(obs_noisy,t) 
        Obs.set_fullRtime(R,t)

#################
### 5. EnKF cycle
#################
  
EnKFstoch        = DAfilt.EnKF_stochastic(testOscil,Obs)
EnKFdeterm       = DAfilt.EnKF_deterministic(testOscil,Obs)
memberstoch      = members_i
memberdeterm     = members_i

for it,t in enumerate(np.arange(2,OBS_STEP+1)): ## DAfull method starts by analysis step. Need to forecast until first observation time step
    memberstoch,TrajEnKFstoch_dic   = testOscil.oneStepForFullEnsemble(t,t+1,memberstoch,traj_dic=TrajEnKFstoch_dic)
    memberdeterm,TrajEnKFdeterm_dic = testOscil.oneStepForFullEnsemble(t,t+1,memberdeterm,traj_dic=TrajEnKFdeterm_dic)

memberstoch,TrajEnKFstoch_dic   = EnKFdeterm.DAfull(OBS_STEP,TEND,memberstoch,traj_dic=TrajEnKFstoch_dic)
memberdeterm,TrajEnKFdeterm_dic = EnKFdeterm.DAfull(OBS_STEP,TEND,memberdeterm,traj_dic=TrajEnKFdeterm_dic)

###########
### 6. Plot
###########

f1,axs1 = plt.subplots(5,2,figsize=(20,20))
TrajOL_dic['position'].plot(ax=axs1[0,0],label="OL",color="blue")
TrajTrue_dic['position'].plot(ax=axs1[0,0],label="True")
TrajTrue_dic['position'].plot(ax=axs1[1,0],label="True")
TrajTrue_dic['position'].plot(ax=axs1[2,0],label="True")
TrajEnKFstoch_dic['position'].plot(ax=axs1[1,0],color="firebrick")
Obs.plot(ax=axs1[0,0],label="Obs",color="green")
Obs.plot(ax=axs1[1,0],label="Obs",color="green")
Obs.plot(ax=axs1[2,0],label="Obs",color="green")


TrajEnKFstoch_dic['position'].plot_mean(ax=axs1[2,0],color="firebrick",linestyle="-",linewidth=2,label="EnKF-Mean")

### RMSE
PerfStoch.plot_RMSE_traj(TrajTrue_dic['position'],ax=axs1[3,0],color="firebrick",label="RMSE-EnKF")
PerfOL.plot_RMSE_traj(TrajTrue_dic['position'],ax=axs1[3,0],color="blue",label="RMSE-OL")

### CRPS
PerfStoch.plot_CRPS_traj(TrajTrue_dic['position'],ax=axs1[4,0],color="firebrick",label="CRPS-EnKF")
PerfOL.plot_CRPS_traj(TrajTrue_dic['position'],ax=axs1[4,0],color="blue",label="CRPS-OL")

axs1[0,0].set_ylabel(' Position [m] ')
axs1[1,0].set_ylabel(' Position [m] ')
axs1[2,0].set_ylabel(' Position [m] ')
axs1[3,0].set_ylabel(' RMSE [m] ')
axs1[4,0].set_ylabel(' CRPS [m] ')
axs1[3,0].set_xlabel(' Time ')
axs1[0,0].set_title(' Stochastic EnKF ')

TrajOL_dic['position'].plot(ax=axs1[0,1],label="OL",color="blue")
TrajTrue_dic['position'].plot(ax=axs1[0,1],label="True")
TrajTrue_dic['position'].plot(ax=axs1[1,1],label="True")
TrajTrue_dic['position'].plot(ax=axs1[2,1],label="True")
TrajEnKFdeterm_dic['position'].plot(ax=axs1[1,1],color="firebrick")
Obs.plot(ax=axs1[0,1],color="green")
Obs.plot(ax=axs1[1,1],color="green")
Obs.plot(ax=axs1[2,1],color="green")
TrajEnKFdeterm_dic['position'].plot_mean(ax=axs1[2,1],color="firebrick",linestyle="-",linewidth=2,label="EnKF-Mean")

### Perf - RMSE
PerfDeterm.plot_RMSE_traj(TrajTrue_dic['position'],ax=axs1[3,1],color="firebrick",label="RMSE-EnKF")
PerfOL.plot_RMSE_traj(TrajTrue_dic['position'],ax=axs1[3,1],color="blue",label="RMSE-OL")

### Perf - CRPS
PerfDeterm.plot_CRPS_traj(TrajTrue_dic['position'],ax=axs1[4,1],color="firebrick",label="CRPS-EnKF")
PerfOL.plot_CRPS_traj(TrajTrue_dic['position'],ax=axs1[4,1],color="blue",label="CRPS-OL")


axs1[3,1].set_xlabel(' Time ')
axs1[0,1].set_title(' Deterministic EnKF ')

for idx in itertools.product(range(5),range(2)):
    print(idx)
    axs1[idx[0],idx[1]].legend(loc="lower left")
    axs1[idx[0],idx[1]].set_xlim([0,TEND])

plt.suptitle(f"Anharmonic oscillator of a material point - Frequency of observations: {OBS_STEP} dt" )
f1.subplots_adjust(left=0.05, bottom=0.075, right=0.98, top=0.9, wspace=0.1,hspace=0.4)

# ~ plt.savefig("test_EnKF.png")
plt.show()


exit()



