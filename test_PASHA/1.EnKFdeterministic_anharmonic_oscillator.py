import os,sys
import numpy as np
import copy
import matplotlib.pyplot as plt

path_PASHA = 'path/to/packagePASHA'
sys.path.append(path_PASHA)

import PASHA.models.anh_oscillator as anh
import PASHA.models.trajectory as trj
import PASHA.assimilation.assim_ens_filter as DAfilt

verbuose=2
np.random.seed(444)

####### SETUP ###########
TEND      = 1000
WW        = 3.5*1e-2 ## omega
LL        = 3*1e-4 ## lambda
x0        = 0
x1        = 1
u0        = np.array([x1,x0])
M         = 20 ### members
N         = 2 ### dimension
SIGMA     = 1 ### error on CI
R_SCAL    = 12 ### obs standard deviation
R         = np.array([[R_SCAL]])
OBS_STEP  = 25 ### obs. frequency
##############################

testOscil = anh.AnharmonicOscillator(W=WW,L=LL)
Obs       = anh.ObsInfo(OBS_STEP=OBS_STEP,R=R)

### 1. Intialise members 
members_i = np.empty((N,M)) 
for i in range(M):
	members_i[:,i] = [comp + np.random.normal(scale = SIGMA) for comp in u0]

### 2. Initialise trajectories 
TrajEnKF_dic  = {"position":trj.Trajectory(state_i=np.flip(members_i),time_i=[0,1],m=M,select_op=[1,0]) }
TrajOL_dic    = {"position":trj.Trajectory(state_i=np.flip(members_i),time_i=[0,1],m=M,select_op=[1,0]) }
TrajTrue_dic  = {"position":trj.Trajectory(state_i=u0[::-1],time_i=[0,1],m=1,select_op=[1,0]) }

### 3. Open Loop 
members = members_i

for it,t in enumerate(np.arange(2,TEND+1)):
    members,TrajOL_dic = testOscil.oneStepForFullEnsemble(t,t+1,members,traj_dic=TrajOL_dic)

### 4. Generate virtual obs 
u = u0

for t in np.arange(2,TEND+1):
    
    ## Forecast (reference model)
    u, TrajTrue_dic = testOscil.oneStep(t,t+1,u,traj_dic=TrajTrue_dic)
    TrajTrue_dic['position'].append(TrajTrue_dic['position'].pick(u),t)
    
    if t % OBS_STEP == 0 : ## Observation available: perform analysis
        
        ## Get virtual observation
        obs_noisy = Obs.noisy(Obs.H(u))
        Obs.append(obs_noisy,t) 
        Obs.set_fullRtime(R,t)

### 5. EnKF cycle 
EnKFdeterm      = DAfilt.EnKF_deterministic(testOscil,Obs)
members         = members_i

for it,t in enumerate(np.arange(2,OBS_STEP+1)): ## DAfull method starts by analysis step. Need to forecast until first observation time step
    members,TrajEnKF_dic = testOscil.oneStepForFullEnsemble(t,t+1,members,traj_dic=TrajEnKF_dic)

members,TrajEnKF_dic = EnKFdeterm.DAfull(OBS_STEP,TEND,members,traj_dic=TrajEnKF_dic)

### 6. Plot
f1,axs1 = plt.subplots(3,1,figsize=(14,7))
TrajOL_dic["position"].plot(ax=axs1[0],label="OL",color="blue")
TrajOL_dic["position"].plot_mean(ax=axs1[0],label="OL",linewidth=1,color="blue")
TrajTrue_dic['position'].plot(ax=axs1[0],label="True")
TrajTrue_dic['position'].plot(ax=axs1[1],label="True")
TrajEnKF_dic['position'].plot(ax=axs1[1],label="EnKF",color="firebrick")
TrajEnKF_dic['position'].plot_mean(ax=axs1[1],color="firebrick",linestyle="-",linewidth=1,label="EnKF")
TrajEnKF_dic['position'].plot_mean(ax=axs1[2],color="firebrick",linestyle="-",linewidth=2,label="EnKF-Mean")
Obs.plot(ax=axs1[0],label="Obs",color="green")
Obs.plot(ax=axs1[1],label="Obs",color="green")

f1.subplots_adjust(left=0.05, bottom=0.075, right=0.98, top=0.95, wspace=0.0,hspace=0.4)
axs1[2].set_xlabel('time [-]')
axs1[0].set_ylabel('position [-]')
axs1[1].set_ylabel('position [-]')
axs1[2].set_ylabel('position [-]')
axs1[0].legend()
axs1[1].legend()
axs1[2].legend()
axs1[0].set_title('Anharmonic oscillator - OpenLoop')
axs1[1].set_title('Anharmonic oscillator - EnKF')
axs1[2].set_title('Anharmonic oscillator - EnKF (mean)')

plt.show()
# ~ plt.savefig("test_EnKF.png")

exit()



