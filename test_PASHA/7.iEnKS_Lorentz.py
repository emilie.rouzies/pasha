import os,sys
import numpy as np
import copy
import matplotlib.pyplot as plt
from decimal import *
import itertools

path_PASHA = 'path/to/packagePASHA'
sys.path.append(path_PASHA)

import PASHA.models.lorenz as lrz
import PASHA.models.trajectory as trj
import PASHA.models.perf_study as perf
import PASHA.assimilation.assim_ens_smoother as DAsmooth

verbuose=2
np.random.seed(444)

#############################
########### SETUP ###########
#############################
TEND      = 2
x0        = 3
y0        = -3
z0        = 21
u0        = np.array([x0,y0,z0])
M         = 20 ### members
N         = 3 ### dimension
SIGMA     = 1 ### error on CI
R_SCAL    = 1 ### obs standard deviation
R         = R_SCAL * np.eye(3)
OBS_STEP  = 0.1 ### obs. frequency
L         = 5
S         = 1
##############################
##############################
##############################

TestLorenz = lrz.Lorenz()
Obs        = lrz.ObsInfo(OBS_STEP=OBS_STEP,R=R)

########################
### 1. Intialise members
######################## 
members_i = np.empty((N,M)) 
for i in range(M):
	members_i[:,i] = [comp + np.random.normal(scale = SIGMA) for comp in u0]

###################################################
### 2. Initialise trajectories and perf. indicators
###################################################
TrajiEnKS_dic = {"x":trj.Trajectory(state_i=members_i[0,:],time_i=[0],m=M,select_op=[1,0,0]), 
                 "y":trj.Trajectory(state_i=members_i[1,:],time_i=[0],m=M,select_op=[0,1,0]) }
TrajOL_dic    = {"x":trj.Trajectory(state_i=members_i[0,:],time_i=[0],m=M,select_op=[1,0,0]) ,
                 "y":trj.Trajectory(state_i=members_i[1,:],time_i=[0],m=M,select_op=[0,1,0]) }
TrajTrue_dic  = {"x":trj.Trajectory(state_i=x0,time_i=[0],m=1,select_op=[1,0,0]) ,
                "y":trj.Trajectory(state_i=y0,time_i=[0],m=1,select_op=[0,1,0]) }

PerfiEnKS_x  = perf.PerfStudy(TrajiEnKS_dic['x'])
PerfiEnKS_y  = perf.PerfStudy(TrajiEnKS_dic['y'])

################
### 3. Open Loop
################ 
members = members_i

for it,t in enumerate(np.arange(0,TEND,TestLorenz.dt)):
    members,TrajOL_dic = TestLorenz.oneStepForFullEnsemble(t,t+TestLorenz.dt,members,traj_dic=TrajOL_dic)

###########################
### 4. Generate virtual obs
########################### 
u = u0
        
for it,t in enumerate(np.arange(0,np.float64(Decimal(TEND+TestLorenz.dt)),np.float64(Decimal(TestLorenz.dt)))):
    
    ## Forecast (reference model)
    u,traj_dic = TestLorenz.oneStep(t,t+TestLorenz.dt,u)
    
    TrajTrue_dic['x'].append(TrajTrue_dic['x'].pick(u),t)
    TrajTrue_dic['y'].append(TrajTrue_dic['y'].pick(u),t)

    if (np.float64(Decimal(t % OBS_STEP))<1e-10) or (np.float64(Decimal(OBS_STEP)-Decimal(t % OBS_STEP))<1e-10):
        ## Get virtual observation
        obs_noisy = Obs.noisy(Obs.H(u))
        Obs.append(obs_noisy,np.float64(Decimal(t)*Decimal(1))) 
        Obs.set_fullRtime(R,np.float64(Decimal(t)*Decimal(1)))


##################
### 5. iEnKS cycle
##################   
iEnKS            = DAsmooth.iEnKS(TestLorenz,Obs,L=L,S=S)
members          = members_i

members,TrajiEnKS_dic = iEnKS.DAfull(0,TEND,members,traj_dic=TrajiEnKS_dic,verbuose=0,optional_analysis=True)
           
###########
### 6. Plot
###########

## Plot x component
f1,axs1 = plt.subplots(4,2,figsize=(18,7))
TrajOL_dic['x'].plot(ax=axs1[0,0],color="blue",label="OL")
TrajOL_dic['x'].plot_mean(ax=axs1[0,0],color="blue",linewidth=2,label="OL")
TrajTrue_dic['x'].plot(ax=axs1[0,0],linewidth=2,color="black",label="True")
TrajTrue_dic['x'].plot(ax=axs1[1,0],linewidth=2,color="black",label="True")
TrajiEnKS_dic['x'].plot(ax=axs1[1,0],color="firebrick",label="iEnKS")
TrajiEnKS_dic['x'].plot_mean(ax=axs1[1,0],color="firebrick",linestyle="-",linewidth=2)
TrajiEnKS_dic['x'].plot_mean(ax=axs1[2,0],color="firebrick",linestyle="-",linewidth=2)
Obs.plot_x(ax=axs1[0,0],label="Obs.",color="green")
Obs.plot_x(ax=axs1[1,0],label="Obs.",color="green")
Obs.plot_x(ax=axs1[2,0],label="Obs.",color="green")
PerfiEnKS_x.plot_RMSE_traj(TrajTrue_dic['x'],ax=axs1[3,0],color="firebrick",label="RMSE")

## Plot y component
TrajOL_dic['y'].plot(ax=axs1[0,1],color="blue",label="OL")
TrajOL_dic['y'].plot_mean(ax=axs1[0,1],linewidth=2,color="blue",label="OL")
TrajTrue_dic['y'].plot(ax=axs1[0,1],linewidth=2,color="black",label="True")
TrajTrue_dic['y'].plot(ax=axs1[1,1],linewidth=2,color="black",label="True")
TrajiEnKS_dic['y'].plot(ax=axs1[1,1],color="firebrick",label="iEnKS")
TrajiEnKS_dic['y'].plot_mean(ax=axs1[1,1],linestyle="-",linewidth=2,color="firebrick",label="iEnKS")
TrajiEnKS_dic['y'].plot_mean(ax=axs1[2,1],linestyle="-",linewidth=2,color="firebrick",label="iEnKS")
Obs.plot_y(ax=axs1[0,1],label="Obs.",color="green")
Obs.plot_y(ax=axs1[1,1],label="Obs.",color="green")
Obs.plot_y(ax=axs1[2,1],label="Obs.",color="green")
PerfiEnKS_y.plot_RMSE_traj(TrajTrue_dic['y'],ax=axs1[3,1],color="firebrick",label="RMSE")

f2,axs2 = plt.subplots(1,1,figsize=(10,10))
for j in np.arange(0,TrajiEnKS_dic['x'].m):
    axs2.plot(TrajiEnKS_dic['x'].traj[:,j],TrajiEnKS_dic['y'].traj[:,j],linewidth=0.5,alpha=0.3,color="firebrick")
axs2.plot(np.mean(TrajiEnKS_dic['x'].traj,axis=1),np.mean(TrajiEnKS_dic['y'].traj,axis=1),linewidth=2,color="firebrick",label="iEnKS")
axs2.plot(np.mean(TrajOL_dic['x'].traj,axis=1),np.mean(TrajOL_dic['y'].traj,axis=1),linewidth=2,color="blue",label="OL")
axs2.plot(TrajTrue_dic['x'].traj,TrajTrue_dic['y'].traj,linewidth=2,color="black",label="True")
axs2.legend()

for idx in itertools.product(range(3),range(2)):
    axs1[idx[0],idx[1]].legend(loc="lower left")
    
axs1[0,0].set_title('x component')
axs1[0,1].set_title('y component')

axs1[-1,0].set_xlim([0,TEND])
axs1[-1,1].set_xlim([0,TEND])

# ~ plt.savefig("test_EnKF.png")
plt.show()


exit()



