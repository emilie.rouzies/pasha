import os,sys
import numpy as np
import copy
import itertools
import matplotlib.pyplot as plt
from decimal import *

path_PASHA = 'path/to/packagePASHA'
sys.path.append(path_PASHA)

getcontext().prec = 6

import PASHA.models.lorenz as lrz
import PASHA.models.trajectory as trj
import PASHA.assimilation.assim_ens_filter as DAfilt
import PASHA.models.perf_study as perf

verbuose=2
np.random.seed(444)

####### SETUP ###########
TEND      = 10
x0        = 3
y0        = -3
z0        = 21
u0        = [x0,y0,z0]
M         = 20 ### members
N         = 3 ### dimension
SIGMA     = 1 ### error on CI
R_SCAL    = 1 ### obs standard deviation
R         = R_SCAL * np.eye(3)
OBS_STEP  = 0.1 ### obs. frequency
##############################

TestLorenz = lrz.Lorenz()
Obs        = lrz.ObsInfo(OBS_STEP=OBS_STEP,R=R)


### 1. Intialise members 
members_i = np.empty((N,M)) 
for i in range(M):
	members_i[:,i] = [comp + np.random.normal(scale = SIGMA) for comp in u0]

### 2. Initialise trajectories and perf. indicators
TrajEnKF_x  = trj.Trajectory(state_i=members_i[0,:],time_i=[0],m=M,select_op=[1,0,0]) 
TrajEnKF_y  = trj.Trajectory(state_i=members_i[1,:],time_i=[0],m=M,select_op=[0,1,0]) 
TrajOL_x    = trj.Trajectory(state_i=members_i[0,:],time_i=[0],m=M,select_op=[1,0,0]) 
TrajOL_y    = trj.Trajectory(state_i=members_i[1,:],time_i=[0],m=M,select_op=[0,1,0]) 
TrajTrue_x  = trj.Trajectory(state_i=x0,time_i=[0],m=1,select_op=[1,0,0]) 
TrajTrue_y  = trj.Trajectory(state_i=y0,time_i=[0],m=1,select_op=[0,1,0]) 

PerfEnKF_x  = perf.PerfStudy(TrajEnKF_x)
PerfEnKF_y  = perf.PerfStudy(TrajEnKF_y)

### 3. Open Loop 
members = members_i

for it,t in enumerate(np.arange(0,TEND,TestLorenz.dt)):
    members,traj_dic = TestLorenz.oneStepForFullEnsemble(t,t+TestLorenz.dt,members)
    
    TrajOL_x.append(TrajOL_x.pick(members),t)
    TrajOL_y.append(TrajOL_y.pick(members),t)

### 4. Generate virtual obs 
u = [x0,y0,z0] ##u0

for it,t in enumerate(np.arange(0,np.float64(Decimal(TEND+TestLorenz.dt)),np.float64(Decimal(TestLorenz.dt)))):
    
    ## Forecast (reference model)
    u,traj_dic = TestLorenz.oneStep(t,t+TestLorenz.dt,u)
    
    TrajTrue_x.append(TrajTrue_x.pick(u),t)
    TrajTrue_y.append(TrajTrue_y.pick(u),t)

    if (np.float64(Decimal(t % OBS_STEP))<1e-10) or (np.float64(Decimal(OBS_STEP)-Decimal(t % OBS_STEP))<1e-10):
        print("obs at time",np.float64(Decimal(t)*Decimal(1)))
        ## Get virtual observation
        obs_noisy = Obs.noisy(Obs.H(u))
        Obs.append(obs_noisy,np.float64(Decimal(t)*Decimal(1))) 
        Obs.set_fullRtime(R,np.float64(Decimal(t)*Decimal(1)))
        
print(Obs.time_obs)

### 5. EnKF cycle   
traj_dic        = {"x":TrajEnKF_x,"y":TrajEnKF_y}
EnKFdeterm      = DAfilt.EnKF_deterministic(TestLorenz,Obs)
members         = members_i
members,traj_dic = EnKFdeterm.DAfull(0,TEND,members,traj_dic,verbuose=2)
           
### 6. Plot
f1,axs1 = plt.subplots(4,2,figsize=(18,7))
f2,axs2 = plt.subplots(1,1,figsize=(7,7))

## Plot x component
TrajOL_x.plot(ax=axs1[0,0])
TrajTrue_x.plot(ax=axs1[0,0],linewidth=2)
Obs.plot_x(ax=axs1[0,0])
TrajTrue_x.plot(ax=axs1[1,0],linewidth=2)
TrajEnKF_x.plot(ax=axs1[1,0])
TrajEnKF_x.plot_mean(ax=axs1[2,0],color="red",linestyle="-",linewidth=2)
PerfEnKF_x.plot_RMSE_traj(TrajTrue_x,ax=axs1[3,0],color="blue")

## Plot y component
TrajOL_y.plot(ax=axs1[0,1])
TrajTrue_y.plot(ax=axs1[0,1],linewidth=2)
Obs.plot_y(ax=axs1[0,1])
TrajEnKF_y.plot(ax=axs1[1,1])
TrajTrue_y.plot(ax=axs1[1,1],linewidth=2)
TrajEnKF_y.plot(ax=axs1[1,1])
TrajTrue_y.plot(ax=axs1[2,1],linewidth=2)
TrajEnKF_y.plot_mean(ax=axs1[2,1],color="firebrick",linewidth=2)
PerfEnKF_y.plot_RMSE_traj(TrajTrue_y,ax=axs1[3,1],color="blue")

axs2.plot(TrajTrue_x.traj,TrajTrue_y.traj,color="black")
axs2.scatter(Obs.obsserie[0,:],Obs.obsserie[1,:])
# ~ plt.savefig("test_EnKF.png")
plt.show()


exit()



