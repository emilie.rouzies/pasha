# -*- coding: utf-8 -*-

'''

 MODULE:        iEnKS_anharmonic_oscillator.py

 AUTHOR(S):     INRAE - Emilie Rouzies - April 2021
                
 PURPOSE:       Perform DA on an anharmonic oscillator model using iEnKS method (Bocquet ad Sakov, 2014)
                Full algorithm is implemented using PASHA package
'''

import os,sys
import numpy as np
import copy
import matplotlib.pyplot as plt

path_PASHA = 'path/to/packagePASHA'
sys.path.append(path_PASHA)

import PASHA.models.anh_oscillator as anh
import PASHA.models.trajectory as trj
import PASHA.models.perf_study as perf
import PASHA.assimilation.assim_ens_smoother as DAsmooth

np.random.seed(444)
verbuose=2

####### SETUP ###########
TEND      = 1000
WW        = 3.5*1e-2 ## omega
LL        = 3*1e-4 ## lambda
x0        = 0
x1        = 1
u0        = np.array([x1,x0])
M         = 10 ### members
N         = 2 ### dimension
SIGMA     = 1 ### error on CI
R_SCAL    = 12 ### obs standard deviation
R         = np.array([[R_SCAL]])
OBS_STEP  = 25 ### obs. frequency
L         = 5 ### DA windows length
S         = 2 ### DA shift

##############################

testOscil = anh.AnharmonicOscillator(W=WW,L=LL)
Obs       = anh.ObsInfo(OBS_STEP=OBS_STEP,R=R)

### 1. Intialise members 
members_i = np.empty((N,M)) 
for i in range(M):
	members_i[:,i] = [comp + np.random.normal(scale = SIGMA) for comp in u0]

### 2. Initialise trajectories and perf. indicators
TrajiEnKS  = {"position":trj.Trajectory(np.flip(members_i),[0,1],M,select_op=[1,0]) }
TrajOL     = {"position":trj.Trajectory(np.flip(members_i),[0,1],M,select_op=[1,0]) }
TrajTrue   = {"position":trj.Trajectory(u0[::-1],[0,1],1,select_op=[1,0]) }

PerfiEnKS  = perf.PerfStudy(TrajiEnKS['position'])

### 3. Virtual observation generation, True and OL forecasts
members = members_i
u       = u0

for it,t in enumerate(np.arange(1,TEND+1)):
         
    ## Forecast (reference model)
    u,trj = testOscil.oneStep(t-1,t,u)
    TrajTrue['position'].append(TrajTrue['position'].pick(u),t)
    
    ## Forecast (Open Loop ensemble)
    members, TrajOL = testOscil.oneStepForFullEnsemble(t-1,t,members,traj_dic=TrajOL)
    
    ## Get virtual observation
    if t % OBS_STEP == 0 : ## Observation available
        
        obs_noisy = Obs.noisy(Obs.H(u))
        Obs.append(obs_noisy,t) 
        Obs.set_fullRtime(R,t)


if verbuose>=1:
    print("time_obs",Obs.time_obs)

### 4. iEnKS 
iEnKS        = DAsmooth.iEnKS(testOscil,Obs,L=L,S=S)
t            = 1
members      = members_i
Ef, TrajiEnKS = iEnKS.DAfull(1,TEND,members,TrajiEnKS,verbuose=2)


### 5. Plot
f1,axs1 = plt.subplots(4,1,figsize=(18,7))
TrajOL['position'].plot(ax=axs1[0],color="blue",label="OL")
TrajOL['position'].plot_mean(ax=axs1[0],linewidth=2,color="blue",label="OL")
TrajTrue['position'].plot(ax=axs1[0],color="black",label="True")
TrajTrue['position'].plot(ax=axs1[1],color="black",label="True")
TrajTrue['position'].plot(ax=axs1[2],color="black",label="True")
TrajiEnKS['position'].plot(ax=axs1[1],color="firebrick",label="iEnKS")
TrajiEnKS['position'].plot_mean(ax=axs1[2],linestyle="-",linewidth=2,color="firebrick",label="iEnKS")
Obs.plot(ax=axs1[0],color="green",label="Obs")
Obs.plot(ax=axs1[1],color="green",label="Obs")
PerfiEnKS.plot_RMSE_traj(TrajiEnKS['position'],ax=axs1[3],color="firebrick",label="iEnKS-RMSE")
axs1[3].set_xlim([0,TEND])
axs1[3].set_xlabel(' Time ')
axs1[0].set_ylabel(' Position ')
axs1[1].set_ylabel(' Position ')
axs1[2].set_ylabel(' Position ')
axs1[3].set_ylabel(' RMSE ')
axs1[0].set_title(' iEnKS \n Open-loop - members trajectories ')
axs1[1].set_title(' iEnKS - members trajectories  ')
axs1[2].set_title(' iEnKS - mean trajectory ')
axs1[3].set_title(' iEnKS - RMSE')

for i in range(4):
    axs1[i].legend()

f1.subplots_adjust(hspace=0.5)

# ~ plt.savefig("test_iEnKS.png")
plt.show()


exit()




