import os, sys
import numpy as np
import copy
import matplotlib.pyplot as plt
import itertools

path_PASHA = 'path/to/packagePASHA'
sys.path.append(path_PASHA)

import PASHA.models.anh_oscillator_joint as anh
import PASHA.models.trajectory as trj
import PASHA.models.perf_study as perf
import PASHA.assimilation.assim_ens_smoother as DAsmooth

np.random.seed(444)
verbuose=2

#########################
####### SETUP ###########
#########################

TEND      = 1000
WW        = 3.5*1e-2 ## omega
LL        = 3*1e-4 ## lambda
x0        = 0
x1        = 1
u0        = np.array([x1,x0,WW,LL])
M         = 10 ### members
N         = 4 ### dimension
SIGMA_CI  = 1 ### error on CI
SIGMA_W   = 1e-3 ### error on W
SIGMA_L   = 1e-5 ### error on L
BIAS_W    = 5e-3 ### bias on W
BIAS_L    = 5e-5 ### bias on L
R_SCAL    = 1 ### obs standard deviation
R         = np.array([[R_SCAL]])
OBS_STEP  = 25 ### obs. frequency
L         = 5 ### DA windows length
S         = 2 ### DA shift

##############################
##############################


testOscil = anh.AnharmonicOscillatorJoint()
Obs       = anh.ObsInfo(OBS_STEP=OBS_STEP,R=R)

########################
### 1. Intialise members
######################## 
members_i = np.empty((N,M)) 
for i in range(M):
	members_i[[0,1],i] = [comp + np.random.normal(scale = SIGMA_CI) for comp in [x1,x0]]
	members_i[2,i]     = WW + BIAS_W + np.random.normal(scale = SIGMA_W)
	members_i[3,i]     = LL + BIAS_L + np.random.normal(scale = SIGMA_L)

###################################################
### 2. Initialise trajectories and perf. indicators
###################################################
TrajiEnKS_pos = trj.Trajectory(np.flip(members_i[[0,1],:]),[0,1],M,select_op=[1,0,0,0]) 
TrajiEnKS_w   = trj.Trajectory(members_i[2,:],[1],M,select_op=[0,0,1,0]) 
TrajiEnKS_l   = trj.Trajectory(members_i[3,:],[1],M,select_op=[0,0,0,1]) 
 
TrajOL_pos = trj.Trajectory(np.flip(members_i[[0,1],:]),[0,1],M,select_op=[1,0,0,0]) 
TrajOL_w   = trj.Trajectory(members_i[2,:],[1],M,select_op=[0,0,1,0]) 
TrajOL_l   = trj.Trajectory(members_i[3,:],[1],M,select_op=[0,0,0,1]) 

TrajTrue_pos = trj.Trajectory([x0,x1],[0,1],1,select_op=[1,0,0,0]) 
TrajTrue_w   = trj.Trajectory([WW,WW],[0,1],1,select_op=[0,0,1,0]) 
TrajTrue_l   = trj.Trajectory([LL,LL],[0,1],1,select_op=[0,0,0,1]) 

PerfiEnKS_pos  = perf.PerfStudy(TrajiEnKS_pos)
PerfiEnKS_w  = perf.PerfStudy(TrajiEnKS_w)
PerfiEnKS_l  = perf.PerfStudy(TrajiEnKS_l)

############################################################
### 3. Virtual observation generation, True and OL forecasts
############################################################
members = members_i
u       = u0
y       = np.array([])
t_obs   = []

for it,t in enumerate(np.arange(1,TEND+1)):
         
    ## Forecast (reference model)
    u,trj = testOscil.oneStep(t-1,t,u)
    TrajTrue_pos.append(TrajTrue_pos.pick(u),t)
    TrajTrue_w.append(TrajTrue_w.pick(u),t)
    TrajTrue_l.append(TrajTrue_l.pick(u),t)
    
    ## Forecast (Open Loop ensemble)
    members, trj = testOscil.oneStepForFullEnsemble(t-1,t,members)
    TrajOL_pos.append(TrajOL_pos.pick(members),t)
    TrajOL_w.append(TrajOL_w.pick(members),t)
    TrajOL_l.append(TrajOL_l.pick(members),t)
    
    ## Get virtual observation
    if t % OBS_STEP == 0 : ## Observation available
        obs_noisy = Obs.noisy(Obs.H(u))
        Obs.append(obs_noisy,t) 
        Obs.set_fullRtime(R,t)

############
### 4. iEnKS
############ 
traj_dic     = {"position":TrajiEnKS_pos,"w":TrajiEnKS_w,"l":TrajiEnKS_l}
iEnKS        = DAsmooth.iEnKS(testOscil,Obs,L=L,S=S)
t            = 1
members      = members_i
Ef, traj_dic = iEnKS.DAfull(1,TEND,members,traj_dic)


###########
### 5. Plot
###########

f1,axs1 = plt.subplots(4,3,figsize=(20,15))
TrajOL_pos.plot(ax=axs1[0,0],label="OL",color="blue")
TrajOL_pos.plot_mean(ax=axs1[0,0],label="OL",linewidth=2,color="blue")
TrajTrue_pos.plot(ax=axs1[0,0],color="black",label="True")
TrajTrue_pos.plot(ax=axs1[1,0],color="black",label="True")
TrajTrue_pos.plot(ax=axs1[2,0],color="black",label="True")
TrajiEnKS_pos.plot(ax=axs1[1,0],color="firebrick",label="iEnKS")
TrajiEnKS_pos.plot_mean(ax=axs1[1,0],color="firebrick",linestyle="-",linewidth=2,label="iEnKS")
TrajiEnKS_pos.plot_mean(ax=axs1[2,0],color="firebrick",linestyle="-",linewidth=2,label="iEnKS")
Obs.plot(ax=axs1[0,0],label="Obs",color="green")
Obs.plot(ax=axs1[1,0],label="Obs",color="green")
PerfiEnKS_pos.plot_RMSE_traj(TrajTrue_pos,ax=axs1[3,0],color="firebrick",label="RMSE")
axs1[3,0].set_xlabel(' Time ')
axs1[0,0].set_ylabel(' Position ')
axs1[1,0].set_ylabel(' Position ')
axs1[2,0].set_ylabel(' Position ')
axs1[3,0].set_ylabel(' RMSE ')
axs1[0,0].set_title(' Trajectoire ')


TrajOL_w.plot(ax=axs1[0,1],color="blue",label="OL")
TrajOL_w.plot_mean(ax=axs1[0,1],color="blue",label="OL",linewidth=2)
TrajTrue_w.plot(ax=axs1[0,1],color="black",label="True")
TrajTrue_w.plot(ax=axs1[1,1],color="black",label="True")
TrajTrue_w.plot(ax=axs1[2,1],color="black",label="True")
TrajiEnKS_w.plot(ax=axs1[1,1],color="firebrick",label="iEnKS")
TrajiEnKS_w.plot_mean(ax=axs1[1,1],color="firebrick",linestyle="-",linewidth=2,label="iEnKS")
TrajiEnKS_w.plot_mean(ax=axs1[2,1],color="firebrick",linestyle="-",linewidth=2,label="iEnKS")
PerfiEnKS_w.plot_RMSE_traj(TrajTrue_w,ax=axs1[3,1],color="firebrick",label="RMSE")
axs1[0,1].set_title(' Paramètre omega ')

TrajOL_l.plot(ax=axs1[0,2],color="blue",label="OL")
TrajOL_l.plot_mean(ax=axs1[0,2],color="blue",label="OL",linewidth=2)
TrajTrue_l.plot(ax=axs1[0,2],color="black",label="True")
TrajTrue_l.plot(ax=axs1[1,2],color="black",label="True")
TrajTrue_l.plot(ax=axs1[2,2],color="black",label="True")
TrajiEnKS_l.plot(ax=axs1[1,2],color="firebrick",label="iEnKS")
TrajiEnKS_l.plot_mean(ax=axs1[1,2],color="firebrick",linestyle="-",linewidth=2,label="iEnKS")
TrajiEnKS_l.plot_mean(ax=axs1[2,2],color="firebrick",linestyle="-",linewidth=2,label="iEnKS")
PerfiEnKS_l.plot_RMSE_traj(TrajTrue_l,ax=axs1[3,2],color="firebrick",label="RMSE")
axs1[0,2].set_title(' Paramètre l ')

for idx in itertools.product(range(4),range(3)):
    print(idx)
    axs1[idx[0],idx[1]].legend(loc="lower left")

axs1[-1,0].set_xlim([0,TEND])
axs1[-1,1].set_xlim([0,TEND])
axs1[-1,2].set_xlim([0,TEND])
# ~ plt.savefig("test_iEnKS.png")
plt.tight_layout()
plt.show()


exit()




