import os,sys
import numpy as np
import scipy.linalg as slinalg
import pickle
import subprocess
from decimal import *

from .config_path import *
sys.path.append(path_PASHA)

getcontext().prec = 6

import PASHA.assimilation.assim_scheme as scheme 

class GenericEnsembleFilter(scheme.DAscheme):
        
    def __init__(self, modelInstance, obsInfo):
        '''
        '''
        super().__init__(modelInstance, obsInfo)
        
        
    def analysis(self, E0, t, verbuose=0, path_log='./analysis.log',Anamorphosis=None):
        '''
        Generic analysis step 
        
        --INPUT--
        E0 (2d-array, float64): matrix that contains an ensemble of m state vectors of dimension n ==> dim (n,m)
        t (scal, float64)     : time index for analysis. Index is used to extract observation from ObsInfo instance
        verbuose (scal, int32): analysis phase verbuosity. Should be 0 (default) if no logs are needed or 1
        '''
        raise NotImplementedError


    def DAfull(self,tini,tend,members,traj_dic={},verbuose=0,cluster=False,path_int="./",path_log="./",correct=True,Anamorphosis=None,**kwargs):
        '''
        Full Filtering Data Assimilation scheme based on Forecast / Analysis alternation
        
        -- INPUT --
        tini, tend (scal, float64) : initial and final times for full DA scheme
        members (2d-array, float64): state vector ensemble to be updated. Dimension is (stateDim,members)
        traj_dic (dict, OPTIONAL)  : trajectories to be updated during the DA scheme
        verbuose (scal, int32)     : analysis phase verbuosity. Should be 0 (default) if no logs are needed or 1
        cluster (bool, OPTIONAL)   : if True, workflow is adapted to be run on a cluster with parallel runs
        correct (bool, OPTIONAL)   : if True, members consistency is checked after analysis  
        anamorphosis (instance of Anamorphosis class, OPTIONAL) : if not None, anamorphosis is performed  
        
        -- RETURN --
        members (2d-array, float64): updated state vector ensemble. Dimension is (stateDim,members)
        traj_dic (dict)            : if given as input, updated trajectories, else: empty dict
        
        '''
        os.chdir(os.path.join(path_PASHA,"PASHA","assimilation"))
        
        DA_CYCLE  = np.int32( Decimal(tend-tini) // Decimal(self.obsInfo.OBS_STEP )) ### Number of cycles

        for cycle in range(DA_CYCLE):
            
            print(f'Enter AD cycle {cycle+1}/{DA_CYCLE}')
            
            tcycle_i = np.float64(Decimal(tini)+Decimal(cycle*self.obsInfo.OBS_STEP))
            tcycle_f = np.float64(Decimal(tini)+Decimal((cycle+1)*self.obsInfo.OBS_STEP))
            
            print(f'AD cycle: from {tcycle_i} to {tcycle_f}')
                
            ### Perform analysis
            if cluster:
                
                with open(os.path.join(path_int,f'ModelInstance.pickle'), 'wb') as filem: ### save model instance to pickle
                    pickle.dump(self.modelInstance, filem)
    
                with open(os.path.join(path_int,f'ObsInfo.pickle'), 'wb') as fileo: ### save ObsInfo instance to pickle
                    pickle.dump(self.obsInfo, fileo)
                
                if Anamorphosis is not None:
                    with open(os.path.join(path_int,'Anamorphosis.pickle'), 'wb') as fileana: ### save Anamorphosis 
                        np.save(anamorphosis, fileana)
                                    
                with open(os.path.join(path_int,'members.npy'), 'wb') as filememb: ### save members 
                    np.save(filememb, members)
                    
                os.chdir(os.path.join(path_PASHA,"PASHA","assimilation"))
                cmd    = f'./launch_jobEnKF_cluster.sh {os.path.join(path_PASHA,"PASHA","assimilation")} {tcycle_i} {self.__class__.__name__} {path_int}' ### launch analysis on a cluster node
                launch = subprocess.run(cmd, shell=True)
                
                with open(os.path.join(path_int,'members.npy'), 'rb') as filembn: ### get members
                    members = np.load(filembn)
                
            else:
                os.chdir(os.path.join(path_PASHA,"PASHA","assimilation"))
                members = self.analysis(members,tcycle_i,verbuose=verbuose,path_log=path_log,Anamorphosis=Anamorphosis)
                
            ## Optional step of variable consistency check
            if correct:
                members = self.modelInstance.check_ensemble_consistency(members)
                            
            ## Append to trajectory(ies)
            for key,traj in traj_dic.items(): 
                traj.append(traj.pick(members),tcycle_i)
            
            ## Perform ensemble forecast
            if self.obsInfo.OBS_STEP % self.modelInstance.dt > 1e-17:
                print(self.obsInfo.OBS_STEP,self.modelInstance.dt,self.obsInfo.OBS_STEP % self.modelInstance.dt)
                print(f'ERROR: OBS_STEP uncompatible with model time step')
                exit()
                
            nbSteps = self.obsInfo.OBS_STEP // self.modelInstance.dt
            members,traj_dic = self.modelInstance.multStepForFullEnsemble(tcycle_i,tcycle_f,members,traj_dic=traj_dic,cluster=cluster,path_log=path_log,**kwargs)
            
        ### Perform final analysis
        if cluster:
            
            with open(os.path.join(path_int,f'ModelInstance.pickle'), 'wb') as filem: ### save model instance to pickle
                pickle.dump(self.modelInstance, filem)

            with open(os.path.join(path_int,f'ObsInfo.pickle'), 'wb') as fileo: ### save ObsInfo instance to pickle
                pickle.dump(self.obsInfo, fileo)
                                
            with open(os.path.join(path_int,'members.npy'), 'wb') as filememb: ### save members 
                np.save(filememb, members)
                
            os.chdir(os.path.join(path_PASHA,"PASHA","assimilation"))
            cmd    = f'./launch_jobEnKF_cluster.sh {os.path.join(path_PASHA,"PASHA","assimilation")} {tcycle_i} {self.__class__.__name__} {path_int}' ### launch analysis on a cluster node
            launch = subprocess.run(cmd, shell=True)
            
            with open(os.path.join(path_int,'members.npy'), 'rb') as filembn: ### get members
                members = np.load(filembn)
            
        else:
            print(f"enter analysis with final tend {tend}")
            os.chdir(os.path.join(path_PASHA,"PASHA","assimilation"))
            members = self.analysis(members,tend,verbuose=verbuose,path_log=path_log)
        
        ## Append to trajectory(ies)
        for key,traj in traj_dic.items(): 
            traj.append(traj.pick(members),tend)
            
        return members,traj_dic
        
    

class EnKF_stochastic(GenericEnsembleFilter):
    
    def __init__(self, modelInstance, obsInfo):
        '''
        '''
        super().__init__(modelInstance, obsInfo)
    
    def analysis(self, E0, t, verbuose=0, path_log='./analysis.log',Anamorphosis=None):
        """
        Performs analysis step of the stochastic Ensemble Kalman Filter.
        This function is adapted from Asch et al. (2016) "Data assimilation, Methods, Algorithms and Applications" chapter 6.3 p160
        
        --- INPUT ---
        E0 (2d-array, float64) : matrix that contains an ensemble of m state vectors of dimension n ==> dim (n,m)
        t (scal, float64)      : time index for analysis. Index is used to extract observation from ObsInfo instance
        verbuose (scal, int32) : Verbuosity. Should be 0 (default) if no logs are needed or 1
        
        --- OUTPUT ---
        E0 (2d-array, float64): updated matrix of the ensemble of m state vectors 
        """
                    
        n      = self.modelInstance.stateDim
        m      = np.shape(E0)[1]
        o      = self.obsInfo.o
        ones_m = np.ones((m,1))
        y  = self.obsInfo.get(t).reshape((self.obsInfo.o,1))
        R = self.obsInfo.R
        
        if verbuose==1:
            with open(path_log, 'a') as f:
                f.write("enter analysis step")
                f.write('members',E0)
                f.write('H',self.obsInfo.matrix)
                f.write('y',y)
                f.write('R',R)
                f.write("n",n)
                f.write("m",m)
                f.write("o",o)
                f.write('')
        
        ## 1. Draw statistically consistent observation set
        y_mb  = np.empty((o,m)) 
        u_mb  = np.empty((o,m)) 
        yflat = y.reshape(self.obsInfo.o)
        for mb in range(m):
            y_mb[:,mb] = self.obsInfo.noisy(yflat,R=R) ## produce perturbed obs
            u_mb[:,mb] = yflat - y_mb[:,mb] ## extract only noise
        
        if verbuose==1:
            with open(path_log, 'a') as f:
                f.write('Observation set')   
                f.write("shape y_mb",np.shape(y_mb))
                f.write("shape u_mb",np.shape(u_mb))
                f.write('')
                
        ## 2. Compute the ensemble mean
        x_mean = E0.dot(ones_m) / m
        u_mean = np.mean(u_mb,axis=1).reshape((self.obsInfo.o,1))
        y_mean = self.obsInfo.H(E0).dot(ones_m) / m
        
        if verbuose==1:
            with open(path_log, 'a') as f:
                f.write('Ensemble mean')
                f.write("x_mean",np.shape(x_mean))
                f.write("u_mean",np.shape(u_mean))
                f.write("y_mean",np.shape(y_mean))
                f.write('')
        
        ## 3. Compute the normalised anomalies
        norm = 1/np.sqrt(m-1)        
        X    = norm * (E0 - np.dot(x_mean,np.transpose(ones_m)) )
        Y    = norm * (self.obsInfo.H(E0) - u_mb - y_mean + u_mean)

        if verbuose==1:
            with open(path_log, 'a') as f:
                f.write('')
                f.write('Final Anomalies')
                f.write('X',X)
                f.write('Y',Y)
                f.write('')
            
        ## 4. Compute the Gain
        Yt = Y.transpose()
        YYt = np.dot(Y,Yt)
        invYYt = np.linalg.inv(YYt)
        XYt = np.dot(X,Yt)
        K = np.dot(XYt,invYYt)
        
        if verbuose==1:
            with open(path_log, 'a') as f:
                f.write('XYt',XYt)
                f.write('YYt',np.dot(Y,Yt))
                f.write('invYYt',invYYt)
                f.write('Gain')
                f.write('K',K)
                f.write('')
            
        ## 5. Update the ensemble
        if verbuose==1:
            with open(path_log, 'a') as f:
                f.write('Update members')
        for j in range(m):
            innov = y_mb[:,j] - self.obsInfo.H(E0[:,j]) 
            if verbuose==1:
                with open(path_log, 'a') as f:
                    f.write('member',j,"innov[0]",innov[0])
                    f.write('member before',E0[:,j])
            E0[:,j] = E0[:,j] + np.dot(K,innov) 
            if verbuose==1:
                with open(path_log, 'a') as f:
                    f.write('member after',E0[:,j])
        
        return E0
        

    


class EnKF_deterministic(GenericEnsembleFilter):
    
    def __init__(self, modelInstance, obsInfo):
        '''
        '''
        super().__init__(modelInstance, obsInfo)
    
    def analysis(self, E0, t, verbuose=0, path_log='./',Anamorphosis=None):
        '''
        Performs analysis step of the deterministic Ensemble Kalman Filter in the ensemble subspace.
        This function is adapted from Asch et al. (2016) "Data assimilation, Methods, Algorithms and Applications" chapter 6.3 p166
        
        --- INPUT ---
        E0 (2d-array, float64) : matrix that contains an ensemble of m state vectors of dimension n ==> dim (n,m)
        t (scal, float64)      : time index for analysis. Index is used to extract observation from ObsInfo instance
        y (1d-array, float64)  : vector of observations to be assimilated
        R (2d-array, float64)  : matrix of observation error
        verbuose (scal, int32) : Verbuosity. Should be 0 (default) if no logs are needed or 1
        
        --- OUTPUT ---
        members_update (2d-array, float64): updated matrix of the ensemble of m state vectors 
        '''

        print(f"enter analysis at time {t}")
        print("with obs",self.obsInfo.get(t))

        n      = self.modelInstance.stateDim
        m      = np.shape(E0)[1]
        o      = self.obsInfo.o
        y      = self.obsInfo.get(t).reshape((self.obsInfo.o,1))
        R      = self.obsInfo.Rtime[t]
        ones_m = np.ones((m,1))
        Im     = np.identity(m)
        U      = np.identity(m) ## arbitrary orthogonal matrix
        
        path_log_full = os.path.join(path_log,f"analysis_t{t}.log")

        if verbuose>=1:
            with open(path_log_full, 'a') as f:
                f.write("enter analysis step \n")
                f.write(f'members {np.shape(E0)} \n')
                f.write(f'H {np.shape(self.obsInfo.matrix)} \n')
                f.write(f'y {np.shape(y)} \n')
                f.write(f'R {np.shape(R)} \n')
                f.write(f'n {n} \n')
                f.write(f"m {m} \n")
                f.write(f"o {o} \n")
                f.write('\n')

        if verbuose>1:
            with open(path_log_full, 'a') as f:
                f.write(f'members {np.isnan(np.sum(E0))} \n')
                f.write(f'H {np.isnan(np.sum(self.obsInfo.matrix))} \n')
                f.write(f'y {np.isnan(np.sum(y))} \n')
                f.write(f'R {np.isnan(np.sum(R))} \n')
                f.write('\n')
                
                    
        # ~ print("before anamorphosis check E0",E0)
        # ~ print('')
        # ~ print("before anamorphosis check y",y)
        # ~ print('')
        # ~ print("before anamorphosis check R",R)
                
        if Anamorphosis is not None:
            print('Go for anamorphosis')
            ## State vector anamorphosis
            for mb in range(m):
                E0[:,mb] = Anamorphosis.anamorphosis_state_2gaussian(E0[:,mb])
            ## Obs vector anamorphosis + obs. error anamorphosis /!\ hypothetize R is diagonal
            y, R = Anamorphosis.anamorphosis_obs_2gaussian(y,R)
            
        # ~ print("after anamorphosis check E0",E0)
        # ~ print('')
        # ~ print("after anamorphosis check y",y)
        # ~ print('')
        # ~ print("after anamorphosis check R",R)
        
        # ~ np.save(os.path.join(path_log,f"members_befanalysis_gaussian_t{t}.npy"),E0) 
        
        ## 1. Compute the ensemble mean
        x_mean = E0.dot(ones_m) / m

        if verbuose>=1:
            with open(path_log_full, 'a') as f:
                f.write(f'x_mean {np.shape(x_mean)} \n')
                f.write('\n')

        if verbuose>1:
            with open(path_log_full, 'a') as f:
                f.write(f'x_mean {np.isnan(np.sum(x_mean))} \n')
                f.write('\n')
        
        ## 2. Compute the normalised anomalies
        norm = 1 / np.sqrt(m-1)
        X    = norm * (E0 - np.dot(x_mean,np.transpose(ones_m)))
        
        if verbuose>=1:
            with open(path_log_full, 'a') as f:
                f.write(f'X anomalies {np.shape(X)} \n')
                f.write('\n')
        
        if verbuose>=1:
            with open(path_log_full, 'a') as f:
                f.write(f'X anomalies {np.isnan(np.sum(X))} \n')
                f.write('\n')
        
        ## 3. Project the ensemble into observation space
        Z = np.empty((o,m),dtype=np.float64)
        for j in range(m):
            Z[:,j] = self.obsInfo.H(E0[:,j])
            
        if verbuose>=1:
            with open(path_log_full, 'a') as f:
                f.write(f'Projection into obs space {np.shape(Z)} \n')
                f.write('\n')
            
        if verbuose>1:
            with open(path_log_full, 'a') as f:
                f.write(f'Projection into obs space {np.isnan(np.sum(Z))} \n')
                f.write('\n')
        
        ## 4. Compute the ensemble mean into observation space
        y_mean = Z.dot(ones_m) / m
        
        if verbuose>=1:
            with open(path_log_full, 'a') as f:
                f.write(f'Mean in obs space {np.shape(y_mean)} \n')
                f.write('\n')
        
        if verbuose>1:
            with open(path_log_full, 'a') as f:
                f.write(f'Mean in obs space {np.isnan(np.sum(y_mean))} \n')
                f.write('\n')
        
        ## 5. Compute the posterior ensemble of perturbations and associated weight 
        Rsqrtm = slinalg.pinv(slinalg.sqrtm(R))
        Zy     = norm * (Z - np.dot(y_mean,np.transpose(ones_m)))
        S      = np.dot(Rsqrtm,Zy)
        Yy     = y - y_mean
        delta  = np.dot(Rsqrtm,Yy)
        St     = S.transpose()
        StS    = np.dot(St,S)
        T      = np.linalg.inv(Im+StS)
        
        if verbuose>=1:
            with open(path_log_full, 'a') as f:
                f.write(f'Rsqrtm {np.shape(Rsqrtm)} \n')
                f.write(f'Zy {np.shape(Zy)}\n')
                f.write(f'Yy {np.shape(Yy)} \n')
                f.write(f'S {np.shape(S)} \n')
                f.write(f'delta {np.shape(delta)} \n')
                f.write(f'StS {np.shape(StS)} \n')
                f.write(f'T {np.shape(T)} \n')
                f.write('\n')
        
        if verbuose>1:
            with open(path_log_full, 'a') as f:
                f.write(f'Rsqrtm {np.isnan(np.sum(Rsqrtm))} \n')
                f.write(f'Zy {np.isnan(np.sum(Zy))}\n')
                f.write(f'Yy {np.isnan(np.sum(Yy))} \n')
                f.write(f'S {np.isnan(np.sum(S))} \n')
                f.write(f'delta {np.isnan(np.sum(delta))} \n')
                f.write(f'StS {np.isnan(np.sum(StS))} \n')
                f.write(f'T {np.isnan(np.sum(T))} \n')
                f.write('\n')

        w1 = np.dot(T,St)
        w  = np.dot(w1,delta)

        if verbuose>=1:
            with open(path_log_full, 'a') as f:
                f.write(f'w1 {np.shape(w1)}')
                f.write(f'w {np.shape(w)}')
                f.write('\n')

        if verbuose>1:
            with open(path_log_full, 'a') as f:
                f.write(f'w1 {np.isnan(np.sum(w1))}')
                f.write(f'w {np.isnan(np.sum(w))}')
                f.write('\n')
        
        ## 6. Update the ensemble
        xmean_new = np.array(x_mean).reshape((n, 1))
        xmean_nm  = np.dot(xmean_new,ones_m.transpose())
        w_new     = np.array(w).reshape((m, 1))
        weight_mm = np.dot(w_new,ones_m.transpose())
        
        if verbuose>=1:
            with open(path_log_full, 'a') as f:
                f.write(f'weight_mm {np.shape(weight_mm)} \n')
        if verbuose>1:
            with open(path_log_full, 'a') as f:
                f.write(f'weight_mm {np.isnan(np.sum(weight_mm))} \n')

        Tsqrm = slinalg.sqrtm(T)
        TU    = np.dot(Tsqrm,U)/norm
        pert  = weight_mm + TU

        if verbuose>=1:
            with open(path_log_full, 'a') as f:
                f.write(f'pert {np.shape(pert)} \n')
                f.write('\n')
        if verbuose>1:
            with open(path_log_full, 'a') as f:
                f.write(f'pert {np.isnan(np.sum(pert))} \n')
                f.write('\n')
        
        members_update = xmean_nm + np.dot(X,pert)
        
        if verbuose==1:
            with open(path_log_full, 'a') as f:
                f.write('Update members \n')
                f.write(f'{np.shape(members_update)} \n')
                
        # ~ np.save(os.path.join(path_log,f"members_aftanalysis_gaussian_t{t}.npy"),members_update)
        
        if Anamorphosis is not None:
            ## State vector anamorphosis
            for mb in range(m):
                members_update[:,mb] = Anamorphosis.anamorphosis_state_2physic(members_update[:,mb])
        
        return np.real(members_update)

        
