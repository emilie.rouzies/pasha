import os, sys
import numpy as np
import copy
import scipy.linalg as slinalg
from decimal import *

from .config_path import *
sys.path.append(path_PASHA)


import PASHA.assimilation.assim_scheme as scheme 
import PASHA.assimilation.assim_ens_filter as DAfilt

class iEnKS(scheme.DAscheme):
    
    def __init__(self, modelInstance, obsInfo, L = 1, S = 1, eps = 0.001, infl = 1., jmax = 10):
        '''
        '''
        super().__init__(modelInstance, obsInfo)
        
        self.L    = L
        self.S    = S
        self.P    = L - S + 1 
        
        self.eps  = eps
        self.infl = infl
        self.jmax = jmax
        
        
    def analysis(self, E0, iac, t, traj_dic={}, optional_analysis=False, verbuose=0, cluster=False, correct=False, path_log="", **kwarg):
        '''
        Performs analysis step of the multiple data assimilation / bundle / Gauss-Newton iterative Ensemble Kalman Smoother.
        This function is adapted from Asch et al. (2016) "Data assimilation, Methods, Algorithms and Applications" chapter 7.4 p
        
        --- INPUT ---
        E0 (2d-array, float64)             : matrix that contains an ensemble of m state vectors of dimension n ==> dim (n,m)
        iac (scal, int32)                  : index of assimilation cycle 
        t (scal, float64)                  : time index for analysis (begining of assimilation window).
        traj_dic (dict, OPTIONAL)          : trajectories to be updated during the DA scheme
        optional_analysis (bool, OPTIONAL) : if True, perform additional forecast within the assimilation window (See Bocquet2013 section 2.1.1) ER 26/05/2021 : a verifier  
        verbuose (scal, int32, OPTIONAL)   : Verbuosity. Should be 0 (default) if no logs are needed or 1
        cluster (bool, OPTIONAL)           : if True, workflow is adapted to be run on a cluster with parallel runs
        
        --- OUTPUT ---
        members_update (2d-array, float64): updated matrix of the ensemble of m state vectors 
        '''
                
        m            = np.shape(E0)[1]
        weight       = np.zeros((m,1))
        ones_m       = np.ones((m,1))

        if verbuose>=0:
            print(f"***********enter assim cycle {iac+1}")
            print(f"=> Time index {t}")
        
        obs_window = self.obsInfo.obsserie[:,iac*self.S:iac*self.S+self.L+1]
        LAG        = self.L
            
        beta  = np.ones(np.shape(obs_window)[1]) * 1. / np.shape(obs_window)[1]
        
        if verbuose>=1:
            print("obs_window",iac*self.S,iac*self.S+LAG) 
            print("obs_window",obs_window) 
            print("obs_window",np.shape(obs_window)) 
            print('beta',beta)
            print('beta',np.shape(beta))
            print('LAG',LAG)
            print('')
        
        ## 1. Compute the ensemble mean
        x_mean = np.dot(E0,ones_m) / m
        
        if verbuose==2:
            print("x_mean",np.shape(x_mean)) 
            print('')
        
        ## 2. Compute the normalised anomalies
        norm = 1/np.sqrt(m-1)
        X0 = norm * (E0 - np.dot(x_mean,np.transpose(ones_m)) )
            
        if verbuose==2:
            print("initial X0",np.shape(X0)) 
            print("norm",norm) 
            print('')
            
        j = 0
        
        ## 3. Perform optimization
        while j < self.jmax:
            
            if verbuose>=2:
                print(f"***iteration {j+1}/{self.jmax}")
                    
            ## Update mean state and ensemble 
            x0_mean = x_mean + np.dot(X0,weight) 
            
            if verbuose>=2:
                print("x0_mean",np.shape(x0_mean)) 
                print('')
    
            ### Rescale ensemble closer to the mean trajectory
            E0 = np.dot(x0_mean,np.transpose(ones_m)) + self.eps*X0
            
            ## Optional step: check of physical consistency of ensemble
            if correct:
                print("enter correct")
                E0 = self.modelInstance.check_ensemble_consistency(E0)
            
            ## Compute the ensemble mean into observation space
            y0_mean = self.obsInfo.H(E0).dot(ones_m) / m
            
            if verbuose>=2:
                print('Mean in obs space y0_mean',np.shape(y0_mean))
                print('')     
    
            ## Compute rescaled anomalies into obs space
            Y0 = (self.obsInfo.H(E0) - np.dot(y0_mean,np.transpose(ones_m))) / self.eps 
            
            if verbuose>=2:
                print('anomalies in obs space',np.shape(Y0))
                print('')  
                
            Xk      = copy.deepcopy(E0)
            tt      = copy.deepcopy(t)
            R       = self.obsInfo.Rtime[tt]
            Rinv    = slinalg.pinv(R)
            y0TRinv = beta[0] * np.matmul(Y0.transpose(),Rinv)
            grad    = np.dot(y0TRinv,(obs_window[:,0].reshape((self.obsInfo.o,1))-y0_mean))
            Hess    = np.dot(y0TRinv,Y0)
            
            for k in range(LAG):
            
                if verbuose>=2:
                    print('tt is',tt)
                    print('k is',k)
                    print('beta[k+1]',beta[k+1])
                    print('')
                    
                R       = self.obsInfo.Rtime[tt]
                Rinv    = slinalg.pinv(R)

                Xk,trj  = self.modelInstance.multStepForFullEnsemble(tt,tt+self.obsInfo.OBS_STEP,Xk,traj_dic={},**kwarg)
                yk_mean = self.obsInfo.H(Xk).dot(ones_m) / m
                yk      = self.obsInfo.H(Xk)-np.dot(yk_mean,np.transpose(ones_m))/self.eps 
                ykTRinv = beta[k+1] * np.matmul(yk.transpose(),Rinv)
                grad+=    np.dot(ykTRinv,(obs_window[:,k+1].reshape((self.obsInfo.o,1))-yk_mean))
                Hess+=    np.dot(ykTRinv,yk)
                tt      = np.float64(Decimal(tt)+Decimal(self.obsInfo.OBS_STEP))
                
                if verbuose>=2:
                    print("grad",np.shape(grad) )
                    print("Xk",np.shape(Xk)) 
                    print("yk_mean",np.shape(yk_mean)) 
                    print("yk",np.shape(yk)) 
                    print("ykTRinv",np.shape(ykTRinv)) 
                    print("obs_window[k+1]",np.shape(obs_window[:,k+1]))
                    print("np.dot(ykTRinv,(obs_window[k+1]-yk_mean))",np.shape(np.dot(ykTRinv,(obs_window[:,k+1].reshape((self.obsInfo.o,1))-yk_mean))))
                    
            if verbuose>=2:
                print('final grad',np.shape(grad))
                print('weight',np.shape(weight))
                
            grad    = weight - grad
            Hess    = np.eye(m) + Hess
            U, s, V = slinalg.svd(Hess, full_matrices=False)
            s       = s ** (-1)
            deltaw  = np.transpose(V).dot(np.diag(s)).dot(np.transpose(U)).dot(grad)
            weight  = weight - deltaw
            
            if verbuose>=2:
                print('Hess',np.shape(Hess))
                print('grad',np.shape(grad))
                print('U,s,V',np.shape(U), np.shape(s), np.shape(V))
                print('deltaw',deltaw)
                print('weight',weight)
            
            j+=1
            
        ### End of while loop ###
            
        sqrtHinv = np.transpose(V).dot(np.diag(np.sqrt(s))).dot(np.transpose(U))
        
        ## 4. Update ensemble
        E0  = np.dot(x0_mean,np.transpose(ones_m)) + (np.dot(X0,sqrtHinv)/norm)
        
        ## Optional step: check of physical consistency of ensemble
        if correct:
            print("enter correct")
            E0 = self.modelInstance.check_ensemble_consistency(E0)
            
        if traj_dic != {}:
            for key,traj in traj_dic.items(): 
                traj.append(traj.pick(E0),t)
    
        ## 5. Optional analysis step . See Bocquet2013 section 2.1.1
        Ei = copy.deepcopy(E0)
        tt = copy.deepcopy(t)
        if (traj_dic != {} and optional_analysis): 
            for i in range(self.S-1):
                print(f"Enter optional analysis step with i={i}") 
                Ei, traj_dic = self.modelInstance.oneStepForFullEnsemble(tt,tt+self.modelInstance.dt,Ei,traj_dic={},**kwarg)
                tt           = np.float64(Decimal(tt)+Decimal(self.modelInstance.dt))
            
        ## 6. Move forward    
        endsmooth_ensble, traj_dic = self.modelInstance.multStepForFullEnsemble(t,np.float64(Decimal(t)+Decimal(self.S)*Decimal(self.obsInfo.OBS_STEP)),E0,traj_dic=traj_dic,cluster=cluster,path_log=path_log,**kwarg)
        
        ## 7. Inflation 
        x0_mean_infl          = endsmooth_ensble.dot(ones_m) / m
        endsmooth_ensble      = np.dot(x0_mean_infl,np.transpose(ones_m)) + self.infl * (endsmooth_ensble - np.dot(x0_mean_infl,np.transpose(ones_m)))
        
        t                     = np.float64(Decimal(t)+Decimal(self.S)*Decimal(self.obsInfo.OBS_STEP))
        
        if verbuose>=1:
            print(f"At the end of the analysis step, time t is {t}")
        
        return E0, t, endsmooth_ensble, traj_dic
            
            
    def DAfull(self, tini, tend, members, traj_dic={}, optional_analysis=False, verbuose=0, cluster=False, correct=False, path_log="./", **kwarg):
        '''
        
        --INPUT--
        traj_dic (dict, OPTIONAL)  : trajectories to be updated during the DA scheme
        verbuose (scal, int32)     : analysis phase verbuosity. Should be 0 (default) if no logs are needed or 1
        cluster (bool, OPTIONAL)   : if True, workflow is adapted to be run on a cluster with parallel runs
        correct (bool, OPTIONAL)   : if True, members consistency is checked after analysis  
        '''
        t = tini
        
        ### 1. Initialize forecast  
        if self.obsInfo.OBS_STEP > tini: ## Initialise temporal ensemble at first obs time (make forecast until first obs is reached)
            while t % self.obsInfo.OBS_STEP != 0 :
                if verbuose>=1:print(f'Initial forecast between {t} and {t+self.modelInstance.dt}')
                members, trj = self.modelInstance.oneStepForFullEnsemble(t,t+self.modelInstance.dt,members,traj_dic={},**kwarg)
                t+=self.modelInstance.dt
                if t % self.obsInfo.OBS_STEP!=0:
                    for key,traj in traj_dic.items(): 
                        traj.append(traj.pick(members),t)
          
        ### 2. iEnKS cycle  
        endCycle     = self.P//self.S
        nbAssimCycle = (self.obsInfo.W-self.P)//self.S   
        if verbuose>=1:
            print("self.obsInfo.W",self.obsInfo.W)
            print("nbAssimCycle",nbAssimCycle)
            print(f"Enter AD loop at time index {t} with members shape")
            print(np.shape(members))
        
        for iac in range(nbAssimCycle):
            print(f"assim cycle iac={iac}")
            updateCI,t,members,traj_dic = self.analysis(members,
                                                        iac,
                                                        t,
                                                        traj_dic=traj_dic,
                                                        optional_analysis=optional_analysis,
                                                        verbuose=verbuose,
                                                        cluster=cluster,
                                                        correct=correct,
                                                        path_log=path_log,
                                                        **kwarg)
            
            if verbuose>=2:
                print("Updated members",members)
                print(members)
            for key,traj in traj_dic.items(): 
                traj.append(traj.pick(members),t)
            
        print("End analysis at time ",t)
        
        ### 3. Final forecast        
        while t<tend:
            members, traj_dic = self.modelInstance.oneStepForFullEnsemble(t,t+self.modelInstance.dt,members,traj_dic=traj_dic,**kwarg)
            t+=self.modelInstance.dt    
            
        return members, traj_dic
    


class EnKS(scheme.DAscheme):
    
        
    def __init__(self, modelInstance, obsInfo, L = 1):
        '''
        '''
        super().__init__(modelInstance, obsInfo)
        
        self.L = L
        self.EnKF = DAfilt.EnKF_deterministic(modelInstance, obsInfo)
        
    def forward_analysis(self,members,t):
        '''
        '''
        return self.EnKF.analysis(members,t)
        
    def DAfull(self,tini,tend,members,traj_dic = {},verbuose=0,**kwargs):
        '''
        '''
        
        members_temporal = copy.deepcopy(members)
        
        ### 1. Foreward EnKF        
        for it,t in enumerate(np.arange(tini,tend)):
            
            ## Perform ensemble forecast
            members = self.modelInstance.oneStepForFullEnsemble(members,**kwarg)
        
            if t % self.obsInfo.OBS_STEP == 0 : ## Observation available: perform analysis

                members = self.forward_analysis(members,t)
                        
                ## Store past analysis in block 
                if members_temporal.ndim==3:
                    members_temporal = np.vstack((members_temporal,members[None]))
                else:
                    members_temporal = np.stack((members_temporal,members))
                  
        ### 2. Backward EnKS 
        reanalysis_members = copy.deepcopy(members_temporal[:,:,:]) ## no reanalysis for last one (actually, yes but we do it again within the backward loop)
        it = -1
                
        while t>tend-self.L:
            
            ### Forecast
            ensemble_time_L = self.modelInstance.multStepForFullEnsemble(members_temporal[it-1,:,:],self.obsInfo.OBS_STEP) 
            
            ### Analysis
            reanalysis_members[it,:,:] = self.analysis(ensemble_time_L,t)
            
            ### Transfer the analysis to remaining time steps
            for i in range(np.shape(members_temporal)[0]+it):
                ensemble_time_l = members_temporal[i,:,:]
                tt = self.obsInfo.time_obs[i] 
                reanalysis_members[i,:,:] = self.analysis(ensemble_time_l,tt)
                
            it-=1
            t = t - self.obsInfo.OBS_STEP




class ES(scheme.DAscheme):
    
        
    def __init__(self, modelInstance, obsInfo, L = 1):
        '''
        '''
        super().__init__(modelInstance, obsInfo)
        
        self.L = L
        self.EnKF = DAfilt.EnKF_deterministic(modelInstance, obsInfo)
        
        
    def analysis_deterministic(self, E0, y, verbuose=0, HM=False, path_log='./',Anamorphosis=None):
        '''
        Performs analysis step of the deterministic Ensemble Kalman Filter in the ensemble subspace.
        This function is adapted from Asch et al. (2016) "Data assimilation, Methods, Algorithms and Applications" chapter 6.3 p166
        
        --- INPUT ---
        E0 (2d-array, float64) : matrix that contains an ensemble of m state vectors of dimension n ==> dim (n,m)
        y (1d-array, float64)  : vector of observations to be assimilated
        R (2d-array, float64)  : matrix of observation error
        verbuose (scal, int32) : Verbuosity. Should be 0 (default) if no logs are needed or 1
        HM (scal,bool)         : if True, the composition of model and observation operator is explicitely used instead of observation operator only
        
        --- OUTPUT ---
        members_update (2d-array, float64): updated matrix of the ensemble of m state vectors 
        '''
        
        np.save(os.path.join(path_log,f"members_prior.npy"),E0)
        
        n      = self.modelInstance.stateDim
        m      = np.shape(E0)[1]
        o      = self.obsInfo.o
        y      = y.reshape((self.obsInfo.o,1))
        R      = self.obsInfo.R
        ones_m = np.ones((m,1))
        Im     = np.identity(m)
        U      = np.identity(m) ## arbitrary orthogonal matrix
        
        path_log_full = os.path.join(path_log,f"analysis.log")
          
        if verbuose==1:
            with open(path_log_full, 'a') as f:
                f.write("enter analysis step \n")
                f.write(f'members {np.shape(E0)} \n')
                f.write(f'H {np.shape(self.obsInfo.matrix)} \n')
                f.write(f'y {np.shape(y)} \n')
                f.write(f'R {np.shape(R)} \n')
                f.write(f'n {n} \n')
                f.write(f"m {m} \n")
                f.write(f"o {o} \n")
                f.write('\n')
                
        if Anamorphosis is not None:
            print('Ok anamorphosis')
            ## State vector anamorphosis
            for mb in range(m):
                E0[:,mb] = Anamorphosis.anamorphosis_state_2gaussian(E0[:,mb])
            ## Obs vector anamorphosis + obs. error anamorphosis /!\ hypothetize R is diagonal
            y, R = Anamorphosis.anamorphosis_obs_2gaussian(y,R)
        
        ## 1. Compute the ensemble mean
        x_mean = E0.dot(ones_m) / m

        if verbuose==1:
            with open(path_log_full, 'a') as f:
                f.write(f'x_mean {np.shape(x_mean)} \n')
                f.write('\n')
        
        ## 2. Compute the normalised anomalies
        norm = 1 / np.sqrt(m-1)
        X    = norm * (E0 - np.dot(x_mean,np.transpose(ones_m)))

        if verbuose==1:
            with open(path_log_full, 'a') as f:
                f.write(f'X anomalies {np.shape(X)} \n')
                f.write('\n')
        
        ## 3. Project the ensemble into observation space
        Z = np.empty((o,m),dtype=np.float64)
        if HM: ## Use HoM instead of H
            print("enter HM")
            for j in range(m):
                Z[:,j] = self.obsInfo.HM(self.modelInstance,E0[:,j],j)
        else: ## Observation Operator only
            print("enter H")
            for j in range(m):
                Z[:,j] = self.obsInfo.H(E0[:,j])
            
        if verbuose==1:
            with open(path_log_full, 'a') as f:
                f.write(f'Projection into obs space {np.shape(Z)} \n')
                f.write('\n')
        
        ## 4. Compute the ensemble mean into observation space
        y_mean = Z.dot(ones_m) / m
        
        if verbuose==1:
            with open(path_log_full, 'a') as f:
                f.write(f'Mean in obs space {np.shape(y_mean)} \n')
                f.write('\n')
        
        ## 5. Compute the posterior ensemble of perturbations and associated weight 
        Rsqrtm = slinalg.pinv(slinalg.sqrtm(R))
        Zy     = norm * (Z - np.dot(y_mean,np.transpose(ones_m)))
        S      = np.dot(Rsqrtm,Zy)
        Yy     = y - y_mean
        delta  = np.dot(Rsqrtm,Yy)
        St     = S.transpose()
        StS    = np.dot(St,S)
        T      = np.linalg.pinv(Im+StS)
        
        if verbuose==1:
            with open(path_log_full, 'a') as f:
                f.write(f'Rsqrtm {np.shape(Rsqrtm)} \n')
                f.write(f'Zy {np.shape(Zy)}\n')
                f.write(f'Yy {np.shape(Yy)} \n')
                f.write(f'S {np.shape(S)} \n')
                f.write(f'delta {np.shape(delta)} \n')
                f.write(f'StS {np.shape(StS)} \n')
                f.write(f'T {np.shape(T)} \n')
                f.write('\n')

        w1 = np.dot(T,St)
        w  = np.dot(w1,delta)

        if verbuose==1:
            with open(path_log_full, 'a') as f:
                f.write(f'w1 {np.shape(w1)}')
                f.write(f'w {np.shape(w)}')
                f.write('\n')
        
        ## 6. Update the ensemble
        xmean_new = np.array(x_mean).reshape((n, 1))
        xmean_nm  = np.dot(xmean_new,ones_m.transpose())
        w_new     = np.array(w).reshape((m, 1))
        weight_mm = np.dot(w_new,ones_m.transpose())
        
        if verbuose==1:
            with open(path_log_full, 'a') as f:
                f.write(f'weight_mm {np.shape(weight_mm)} \n')

        Tsqrm = slinalg.sqrtm(T)
        TU    = np.dot(Tsqrm,U)/norm
        pert  = weight_mm + TU

        if verbuose==1:
            with open(path_log_full, 'a') as f:
                f.write(f'pert {np.shape(pert)} \n')
                f.write('\n')
        
        members_update = xmean_nm + np.dot(X,pert)
        
        if verbuose==1:
            with open(path_log_full, 'a') as f:
                f.write('Update members \n')
                f.write(f'{np.shape(members_update)} \n')

        if Anamorphosis is not None:
            ## State vector anamorphosis
            for mb in range(m):
                members_update[:,mb] = Anamorphosis.anamorphosis_state_2physic(members_update[:,mb])
                
        if verbuose==1:
            np.save(os.path.join(path_log,f"x_mean.npy"),x_mean)
            np.save(os.path.join(path_log,f"X.npy"),X)
            np.save(os.path.join(path_log,f"Z.npy"),Z)
            np.save(os.path.join(path_log,f"y_mean.npy"),y_mean)
            np.save(os.path.join(path_log,f"Rsqrtm.npy"),Rsqrtm)
            np.save(os.path.join(path_log,f"Zy.npy"),Zy)
            np.save(os.path.join(path_log,f"Yy.npy"),Yy)
            np.save(os.path.join(path_log,f"delta.npy"),delta)
            np.save(os.path.join(path_log,f"StS.npy"),StS)
            np.save(os.path.join(path_log,f"T.npy"),T)
            np.save(os.path.join(path_log,f"weight.npy"),w_new)
            np.save(os.path.join(path_log,f"Tsqrm.npy"),Tsqrm)
            np.save(os.path.join(path_log,f"pert.npy"),pert)
            np.save(os.path.join(path_log,f"members_posterior.npy"),members_update)

            
            ## 3. Project the posterior ensemble into observation space
            Zpost = np.empty((o,m),dtype=np.float64)
            if HM: ## Use HoM instead of H
                for j in range(m):
                    Zpost[:,j] = self.obsInfo.HM(self.modelInstance,members_update[:,j],j)
            else: ## Observation Operator only
                for j in range(m):
                    Zpost[:,j] = self.obsInfo.H(members_update[:,j])
            np.save(os.path.join(path_log,f"Zpost.npy"),Zpost)

        return members_update
        
        
    def analysis_stochastic(self, E0, y, verbuose=0, HM=False, path_log='./',Anamorphosis=None):
        """
        Performs analysis step of the stochastic Ensemble Kalman Filter.
        This function is adapted from Asch et al. (2016) "Data assimilation, Methods, Algorithms and Applications" chapter 6.3 p160
        
        --- INPUT ---
        E0 (2d-array, float64) : matrix that contains an ensemble of m state vectors of dimension n ==> dim (n,m)
        t (scal, float64)      : time index for analysis. Index is used to extract observation from ObsInfo instance
        verbuose (scal, int32) : Verbuosity. Should be 0 (default) if no logs are needed or 1
        
        --- OUTPUT ---
        E0 (2d-array, float64): updated matrix of the ensemble of m state vectors 
        """
        
        np.save(os.path.join(path_log,f"members_prior.npy"),E0)

        n      = self.modelInstance.stateDim
        m      = np.shape(E0)[1]
        o      = self.obsInfo.o
        y      = y.reshape((self.obsInfo.o,1))
        R      = self.obsInfo.R
        ones_m = np.ones((m,1))
        ones_o = np.ones((o,1))

        path_log_full = os.path.join(path_log,f"analysis.log")
        
        ## 1. Draw statistically consistent observation set
        y_mb  = np.empty((o,m)) 
        u_mb  = np.empty((o,m)) 
        yflat = y.reshape(self.obsInfo.o)
        for mb in range(m):
            y_mb[:,mb] = self.obsInfo.noisy(yflat,R=R) ## produce perturbed obs
            u_mb[:,mb] = yflat - y_mb[:,mb] ## extract only noise
        
        u_mean = np.mean(u_mb,axis=1).reshape((self.obsInfo.o,1))
        
            
        ## 2. Compute the ensemble mean
        xb_bar   = E0.dot(ones_m) / m
        Xb_prime = E0 - np.dot(xb_bar,np.transpose(ones_m)) 
        
        np.save(os.path.join(path_log,f"Xb_prime.npy"),Xb_prime)
        
        
        Z    = np.empty((o,m),dtype=np.float64)   
  
        if HM: ## Use HoM instead of H
            print("enter HM")
            for j in range(m):
                Z[:,j] = self.obsInfo.HM(self.modelInstance,E0[:,j],j)
            
        else: ## Observation Operator only
            print("enter H")
            for j in range(m):
                Z[:,j] = self.obsInfo.H(E0[:,j])
            Zbar = self.obsInfo.H(xb_bar)

        norm    = 1/(m-1)        
        Z_diff  = Z - np.dot(Zbar,np.transpose(ones_m)) 
        z_diffT = Z_diff.transpose() 
        Pb_hat_times_t_H = norm * np.dot(Xb_prime,z_diffT)
        Pb_hat_times_H_times_t_H = norm * np.dot(Z_diff,z_diffT)
        
        invYYt = np.linalg.inv(Pb_hat_times_H_times_t_H + R)
    
        K_hat  = np.dot(Pb_hat_times_t_H,invYYt)
        Ea     = E0 + np.dot(K_hat,(y_mb-Z))  
        
        if verbuose==1:
            np.save(os.path.join(path_log,f"members_posterior.npy"),Ea)
            
        if verbuose==1:
            np.save(os.path.join(path_log,f"xb_bar.npy"),xb_bar)
            np.save(os.path.join(path_log,f"Z.npy"),Z)
            np.save(os.path.join(path_log,f"Z_diff.npy"),Z_diff)
            np.save(os.path.join(path_log,f"Zbar.npy"),Zbar)
            np.save(os.path.join(path_log,f"Pb_hat_times_t_H.npy"),Pb_hat_times_t_H)
            np.save(os.path.join(path_log,f"Pb_hat_times_H_times_t_H.npy"),Pb_hat_times_H_times_t_H)
            np.save(os.path.join(path_log,f"invYYt.npy"),invYYt)
            np.save(os.path.join(path_log,f"K_hat.npy"),K_hat)
            np.save(os.path.join(path_log,f"members_posterior.npy"),Ea)
            np.save(os.path.join(path_log,f"y.npy"),y)
            np.save(os.path.join(path_log,f"R.npy"),R)
        
        return Ea
        
    def DAfull(self,members,obs_flat,verbuose=0,cluster=False,path_int="./",path_log="./",**kwargs):
        '''
        Full Smoothing Data Assimilation scheme 
        Such methods allows the user for choosing to run analysis either on local machine or on cluster
        
        -- INPUT --
        members (2d-array, float64)  : state vector ensemble to be updated. Dimension is (stateDim,members)
        obs_flat (1d-array, float64) : obs values
        verbuose (scal, int32)       : analysis phase verbuosity. Should be 0 (default) if no logs are needed or 1
        cluster (bool, OPTIONAL)     : if True, workflow is adapted to be run on a cluster with parallel runs
        path_int (str, OPTIONAL)     : path for saving variables as npy if cluster is used
        path_log (str, OPTIONAL)     : path for saving logs
        
        -- RETURN --
        members (2d-array, float64): updated state vector ensemble. Dimension is (stateDim,members)

        '''
        os.chdir(os.path.join(path_PASHA,"PASHA","assimilation"))
                
        ### Perform analysis
        if cluster:
            
            with open(os.path.join(path_int,f'ModelInstance.pickle'), 'wb') as filem: ### save model instance to pickle
                pickle.dump(self.modelInstance, filem)

            with open(os.path.join(path_int,f'ObsInfo.pickle'), 'wb') as fileo: ### save ObsInfo instance to pickle
                pickle.dump(self.obsInfo, fileo)
                                
            with open(os.path.join(path_int,'members.npy'), 'wb') as filememb: ### save members 
                np.save(filememb, members)
                                
            with open(os.path.join(path_int,'obs_flat.npy'), 'wb') as filememb: ### save obs value 
                np.save(filememb, obs_flat)
                
            os.chdir(os.path.join(path_pyAD,"pyAD","assimilation"))
            cmd    = f'./launch_jobES_cluster.sh {os.path.join(path_pyAD,"pyAD","assimilation")} {self.__class__.__name__} {path_int} {path_log}' ### launch analysis on a cluster node
            launch = subprocess.run(cmd, shell=True)
            
            with open(os.path.join(path_int,'members.npy'), 'rb') as filembn: ### get members
                members = np.load(filembn)
        
        else:
            os.chdir(os.path.join(path_pyAD,"pyAD","assimilation"))
            members = self.analysis(members,obs_flat,verbuose=1,path_log=path_log)




    
