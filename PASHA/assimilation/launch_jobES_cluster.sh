#!/bin/bash    

### input 1 is code directory
### input 2 is is analysis time 
### input 3 is type of DA method
### input 4 is path to intermediary files for cluster

cd ${1}

JOBIDF=$(sbatch --parsable --array=0-0%50 ./launch_ESstandalone_cluster.sh $1 $2 $3 $4)

echo "${JOBIDF} analysis job"
until (($(scontrol show job $JOBIDF | grep -c JobState=COMPLETED)==1))
do
   sleep 5 
   echo "Analysis still on going"
done

echo "Analysis step over"
