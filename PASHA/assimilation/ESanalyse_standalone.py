import os,sys
import numpy as np
import pickle

from config_path import *
sys.path.append(path_pyAD)
import pyAD.assimilation.assim_ens_smoother as DAsmooth

map_DA = {"ES":DAsmooth.ES,
          "iEnKS":DAsmooth.iEnKS,
          "EnKS":DAsmooth.EnKS}

def main(typeDA,path,path_log):
    """
    
    """
    print(f'begin pickle import...') 
    ### read pickles
    with open(os.path.join(path,f'ModelInstance.pickle'), 'rb') as filem:
        ModeleInstance = pickle.load(filem)
    with open(os.path.join(path,f'ObsInfo.pickle'), 'rb') as fileo:
        ObsInfo = pickle.load(fileo)
    with open(os.path.join(path,'members.npy'), 'rb') as filembn: ### get members
        members = np.load(filembn)
    with open(os.path.join(path,'obs_flat.npy'), 'rb') as fileobs: ### get obs
        obs_flat = np.load(fileobs)
    print(f'end pickle import...')  
  
    ### Initialize DA method
    print(f'initialize DA instance with {typeDA}...') 
    DAmethod = map_DA[typeDA](ModeleInstance,ObsInfo)
    
    ### Perform single analysis
    print(f'begin analysis...') 
    members  = DAmethod.analysis(members,obs_flat,verbuose=1,path_log=path_log)
    print(f'...end analysis') 
    
    ### Save members
    with open(os.path.join(path,'members.npy'), 'wb') as filememb: ### save members 
        np.save(filememb, members)
        
    return 0


        
if __name__ == "__main__":
    main(np.float64(sys.argv[1]),sys.argv[2],sys.argv[3])
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
"""

    MAIN

"""
