#!/bin/bash
#SBATCH --job-name=missingfrcst_PESHMELBA


## Get simulation number
filename=simu_not_ok.txt
analyse_path=${1}
echo $analyse_path/$filename
echo ${SLURM_ARRAY_TASK_ID}
sampleidx=$(sed "${SLURM_ARRAY_TASK_ID}q;d" $analyse_path/$filename)

MEMBERLAUNCH=${1}/${sampleidx}/PESHMELBA/
EX_DIR=${1}/${sampleidx}

echo $MEMBERLAUNCH
echo ${2}
echo ${3}

## Get code src and untar
cd $EX_DIR
tar zxvf PESHMELBA.tar.gz

## Compile PESHMELBA
BASE_DIR=/nfs/home/emilie.rouzies/peshmelba

### Step 1: set config_param files
echo "$EX_DIR/" > $EX_DIR/PESHMELBA/config_param.txt
echo  "folder=\"$EX_DIR\"" > $EX_DIR/PESHMELBA/config_param.py

### Step 2: copy Make.include 
cp $BASE_DIR/Make.include_mpi1 $EX_DIR/PESHMELBA/Make.include

### Step 2: copy Make.include 
cp $BASE_DIR/Make.include_mpi1 $EX_DIR/PESHMELBA/Make.include

### Step 3: change python path
cd $EX_DIR/PESHMELBA

for filename in main_*.py; do
    echo filename
    sed -i '1d' $filename 
    cat $BASE_DIR/pythonpath $filename > buffer
    mv buffer $filename
done

cd ROSS03
make clean
cd ..
make clean

source /nfs/home/peshmelba/chantilly/bin/activate
module load mpich/ge/gcc/64/3.2
make
./run_mpi1.sh 
deactivate 
    

