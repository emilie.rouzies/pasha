#!/usr/bin/python
# -*- coding: utf-8 -*-


import numpy as np
import sys,os,shutil
import random
import subprocess
import copy
from scipy.stats import norm
import matplotlib.pyplot as plt
import gc
import pickle
import copy

from .config_path import *

sys.path.append(path_pyAD) 
import pyAD.models.model_instance as mi
import pyAD.models.obs_info as obsinf
import pyAD.models.trajectory as trj

sys.path.append(path_pyPESHMELBA)
import PyPESHMELBA.py_peshmelba as pypesh


class PESHMELBA(mi.ModelInstance):
    
    def __init__(self,path,estim_param,begin_date,dt=1,stateDim=1,m=1,list_HU=None,fromscratch=True):
        '''
        Constructor
        
        -- INPUT --
        path (str)                       : generic path where members simulations are stored
        estim_param (str, 1d-array)      : parameter names of param to be estimated by AD. Param name syntax should be consistent with sensiPESH syntax ! EX: thetas_3 refeers to thetas in soil horizon 3
        dt (scal,float64,OPTIONAL)       : base time step of model
        stateDim (scal,int32,OPTIONAL)   : dimension of state vector for DA
        m (scal,int32,OPTIONAL)          : number of members /!\ ER: les fichiers d'entree doivent deja exister!
        list_HU (int32,1d-array,OPTIONAL): list of landscape element (ER: not only HU?) /!\ to be estimated /!\
        fromscratch (bool, OPTIONAL)     : of True, members directory are created from scratch. If not, exisitng code is reused 
        '''
        mi.ModelInstance.__init__(self,stateDim=stateDim,m=m,dt=dt)
        
        self.path         = path 
        self.list_HU      = list_HU
        self.estim_param  = estim_param
        self.PESHMELBA_mb = {}
        if fromscratch:
            for i in range(m):
                path_full  = os.path.join(path,"MEMBERS",f"{i}")
                if os.path.exists(path_full): shutil.rmtree(path_full, ignore_errors=True) ### Remove existing directory
                os.makedirs(path_full)
                os.makedirs(os.path.join(path_full,'INPUT'))
                os.makedirs(os.path.join(path_full,'OUTPUT'))
                os.makedirs(os.path.join(path_full,'PESHMELBA'))
                
        self.begin_date = begin_date
            
    def create_EnsembleInstances(self):
        '''
        Generates an instance pf PyPESHMELBA class for each member
        '''
        for i in range(self.m):
            path_full  = os.path.join(self.path,"MEMBERS",f"{i}")
            self.PESHMELBA_mb[i] = pypesh.PyPESHMELBA(path_full)
            
    def getPESHcode_ForFullEnsemble(self,path_src):
        '''
        Get PESHMELBA code from .tar.gz archive and paste it into each member path
        
        --INPUT--
        path_src (str): absolute path to sources for PESHMELBA code stored as .tar.gz archive 
        '''
        for i in range(self.m):
            path_full  = os.path.join(self.path,"MEMBERS",f"{i}")
            os.chdir(os.path.join(path_pyPESHMELBA,'PyPESHMELBA'))
            # ~ cmd    = f'./get_srcPESH.sh {path_src} {path_full}'
            # ~ launch = subprocess.run(cmd, shell=True)
            cmd    = ["./get_srcPESH.sh",f"{path_src}",f"{path_full}"]
            launch = subprocess.call(cmd) 
            
    def clean_DAinterfaceEnsemble(self):
        '''
        Clean repertory INPUT/DAinterface for each member of ensemble
        '''
        for mb in range(self.m):
            self.PESHMELBA_mb[mb].clean_DAinterface()
            
    def clean_TemporalSerieEnsemble(self):
        '''
        Clean repertory INPUT/temporal_series for each member of ensemble
        '''
        for mb in range(self.m):
            self.PESHMELBA_mb[mb].clean_TemporalSerie()
            
    def clean_log(self):
        '''
        Clean LOGS repertory
        '''
        path_logs = os.path.join(self.path,'LOGS')
        if os.path.exists(path_logs):
            shutil.rmtree(path_logs, ignore_errors=True)
        os.makedirs(path_logs) 

    def get_theta_ForFullEnsemble(self):
        '''
        Extract moisture vector from INPUT directory for each member.
        
        --RETURN--
        theta_dic (dict): dict of moisture vectors for each member (mb) and each hu frol self.list_HU (hu). Generix syntax is theta_dic[mb][hu] = 1d-array
        '''
        theta_dic = {}
        for mb in range(self.m):
            theta_dic[mb] = self.PESHMELBA_mb[mb].get_theta_npy(self.list_HU)
            
        return theta_dic

    def get_thetapkl_ForFullEnsemble(self):
        '''
        Extract moisture vector from INPUT directory for each member.
        
        --RETURN--
        theta_dic (dict): dict of moisture vectors for each member (mb) and each hu frol self.list_HU (hu). Generix syntax is theta_dic[mb][hu] = 1d-array
        '''
        theta_dic = {}
        for mb in range(self.m):
            theta_dic[mb] = self.PESHMELBA_mb[mb].get_theta_pkl(self.list_HU)
            
        return theta_dic

    def get_thetatry_ForFullEnsemble(self):
        '''
        Extract moisture vector from INPUT directory for each member. Extraction is first tried in pkl but if no output are found, it is try in npy
        If npy also fails, get moisture for member before or after
        
        --RETURN--
        theta_dic (dict): dict of moisture vectors for each member (mb) and each hu frol self.list_HU (hu). Generix syntax is theta_dic[mb][hu] = 1d-array
        '''
        
        theta_dic = {}
        for mb in range(self.m):

            result = self.PESHMELBA_mb[mb].get_theta_npy(self.list_HU)
            
            if isinstance(result,Exception): ### explore all members until finding one that has been written properly
                
                print(f'error occurs to get theta for member {mb}')

                for mbb in range(self.m):
                    
                    result = self.PESHMELBA_mb[mbb].get_theta_npy(self.list_HU)                        
                    
                    if not isinstance(result,Exception):
                        print(f'Member {mb} replace by nan {mbb}')
                        Nan_array     = np.empty(np.shape(result)) 
                        print(np.shape(Nan_array))
                        if np.shape(Nan_array) != ():
                            Nan_array[:] = np.NaN
                        else:
                            Nan_array = np.NaN
                        theta_dic[mb] = Nan_array
                        break

            else:
                theta_dic[mb] = result

        return theta_dic
        
    def set_theta_ForFullEnsemble(self,members):
        '''
        Pick theta values from ensemble 2d-array for each HU on which state is to be estimated
        Store it into .npy file in DAinterface repertory
        /!\ Only real part is selected !
        
        --INPUT--
        members (2d-array, float64): block of members state vectors. Dimensions are (stateDim,m) 
        '''
        for mb in range(self.m):
            for ihu,hu in enumerate(self.list_HU):
                theta_v = members[ihu*self.PESHMELBA_mb[mb].n_layer:(ihu+1)*self.PESHMELBA_mb[mb].n_layer,mb].real 
                self.PESHMELBA_mb[mb].initialize_theta_npy(hu,theta_v)
                
    def build_hydro_ppte(self,mb):
        '''
        Very HOMEMADE method to build a vector with theta_s value (resp. theta_r value) for each cell of state vector that is a moisture on a given HU
        
        --INPUT--
        mb (scal, int32): member index
        '''
        
        ## 1. Get UCS composition and UCS for each HU (/!\ to do so, we hypothetize that all members have the same UCS composition)
        UCS_dic  = self.PESHMELBA_mb[mb].UCS_dic 

        thetar_dic = {}
        thetas_dic = {}
        
        for hu in self.list_HU:
            thetas_dic[hu],thetar_dic[hu] = self.PESHMELBA_mb[mb].get_hydroppte_4cell(hu)
            
        return thetas_dic,thetar_dic
            
                
    def update_forcing_ForFullEnsemble(self,ti,tf,folder_forcing):
        '''
        Update forcings for each member 
        Individual method is called to pick up forcing time series from a reference file between two dates
        A new truncated forcing file is created with length (tend-tini)
        
        --INPUT--
        ti,tf (scal, float64): initial and final times for truncation
        folder_forcing (str): path to reference time serie from which a truncated serie will be extracted
        '''
        for mb in range(self.m):
            self.PESHMELBA_mb[mb].update_forcing(ti,tf,folder_forcing,self.begin_date)
                
    def oneStepForFullEnsemble(self,ti,tf,members=None,traj_dic={},cluster=False,**kwarg):
        '''
        Method for ensemble forecast for one time step (ti to tf).
        Parameters are supposed to follow a persistence model: values are not changed during the forecast step
                
        --INPUT--
        ti,tf (scal, float64): initial and final times for simulation
        members (float64,2d-array, OPTIONAL): block of state vectors for all members. dim = (state vector length, number of members) 
                                              If defined, values from mebers are used to update PESHMELBA CI and parameters
                                              After ensemble forecast, state variables are updated   
        folder_forcing (str) : path to entire forcing time series from which a truncated time series will be extracted
        cluster (bool)       : if True, launching is done in serie for cluster
        '''
        print(f'Enter OnestepForFullEnsemble between {ti} and {tf}')
        
        ### 1. Update forcings time serie
        self.update_forcing_ForFullEnsemble(ti,tf,kwarg['folder_forcing'],self.begin_date)
        self.clean_TemporalSerieEnsemble()
            
        if members is not None:
            ### 2. Update moisture vect
            self.set_theta_ForFullEnsemble(members)

            ### 3. Update parameters
            for mb in range(self.m):
                for ipr,prm in enumerate(self.estim_param):
                    param    = prm.split("_") ## cut into paramtype and unit (soil horizon, vegettype,...)
                    dict_prm = {param[0]:members[len(self.list_HU)*self.PESHMELBA_mb[mb].n_layer+ipr,mb]}
                    if param[0] in ["thetas","thetar","bd","hg","Ks","Kx","mn","moc","pore"]: ## soil parameter
                        self.PESHMELBA_mb[mb].set_soilhorizon_param(np.int32(param[1]),dict_prm)
                    elif param[0] in ["manning","LAImin","LAImax","LAIharv","F10","Zr","LAI"]: ##veget param
                        self.PESHMELBA_mb[mb].set_veget_param(np.int32(param[1]),dict_prm)
                    ### TODO: alternative param to be coded
        
        ### 4. Launch PESHMELBA 
        typesimu = kwarg['typesimu']
        if cluster:
            print("call peshmelba forecast with external bash command")
            path_mb  = os.path.join(self.path,'MEMBERS')
            os.chdir(os.path.join(path_pyAD,'pyAD','models'))
            # ~ cmd        = f'./peshmelba_forecast_ensemble.sh {self.m} {path_mb} {typesimu} {ti}'
            # ~ launch     = subprocess.run(cmd, shell=True)
            cmd        = [f'./peshmelba_forecast_ensemble.sh', f'{self.m}', f'{path_mb}', f'{typesimu}', f'{ti}']
            launch     = subprocess.call(cmd)   
            
            simu_notok = self.external_cluster_check()
            print('missing simu are',simu_notok)
            if len(simu_notok)>0:
                for nok in simu_notok:
                    path_nok = os.path.join(self.path,'MEMBERS',f'{nok}')
                    # ~ cmd      = f'./launchOpenPALM_front.sh {path_nok}'
                    # ~ launch   = subprocess.run(cmd, shell=True)
                    cmd      = [f'./launchOpenPALM_front.sh', f'{path_nok}']
                    launch   = subprocess.call(cmd) 
                
        else:
            for mb in range(self.m):
                path_log = os.path.join(self.path,'LOGS',f"{typesimu}{mb}_t{ti}.log")
                print(f'    Launch member simulation {mb+1}/{self.m}')
                self.PESHMELBA_mb[mb].launch(path_log)
                flag = self.PESHMELBA_mb[mb].check_simu()
                if not flag:
                    print(f'/!\ Warning /!\ Simulation for member {mb} did not end properly ! ')
                    exit()
                print(f'    End of member simulation {mb+1}/{self.m}')
        
        ### 5. Get new values for theta vector
        theta_dic = self.get_theta_ForFullEnsemble()
        
        ### 6. Update members
        if members is not None:
            for mb in range(self.m):
                for ihu,hu in enumerate(self.list_HU) : ## mantain order !!
                    members[ihu*self.PESHMELBA_mb[mb].n_layer:(ihu+1)*self.PESHMELBA_mb[mb].n_layer,mb] = theta_dic[mb][hu]
                    
        ### 7. Fill trajectories in a different way (need to read ext file)
        for key,traj in traj_dic.items():
            if "var_theta" in key:
                time_traj, value_traj = traj.read_theta_fromtxt() 
                traj.append(value_traj,time_traj+ti)
            
        ### 8. Store thetas profile
        if ('prt_theta' in kwarg) and kwarg['prt_theta'] and kwarg['list_print']!=[]:
            print("save theta")
            
            path_save = os.path.join(self.path,"OUTPUT_theta")
            
            if not os.path.exists(path_save):
                os.makedirs(path_save)
                
            path_save_time = os.path.join(self.path,"OUTPUT_theta",f'{ti}-{tf}')
            if not os.path.exists(path_save_time):
                os.makedirs(path_save_time)
                
            time_ref, theta = self.PESHMELBA_mb[mb].get_theta_txt(self.list_HU[0]) 
            lentime         = len(time_ref)
            n_layer         = np.shape(theta)[1]
            
            for col in kwarg['list_print']:
                theta_block = np.empty((lentime,n_layer,self.m))
                for mb in range(self.m):
                    time,theta   = self.PESHMELBA_mb[mb].get_theta_txt(col)
                    theta_block[:,:,mb] = theta
                
                with open(os.path.join(path_save_time,f'thetablock_HU{col}.npy'),'wb') as f:
                    np.save(f,theta_block)
            
            with open(os.path.join(path_save_time,f'time_ref.npy'),'wb'):
                np.save(f,time_ref)
            
        gc.collect()

        return members,traj_dic
            
    def init_ensemble(self,ti,tf,names,X,heads_dic,folder_forcing,traj_dic={},**kwarg):
        '''
        Very homemade method to initialize members
        Create a state vector with succession of thetas vector and parameters
        
        -- INPUT --
        ti,tf (float64)           : initial and final time for PESH. simulation
        names (str, 1d-array)     : list of parameters sampled in X variable
        X (float64, 2d array)     : parameters PLEX, dim=(number of param, number of members)
        heads_dic (dict)          : dictionnary of heads vector keys are members (mb) and hu idx (hu) , value are 1d array of heads vector/ 
                                    Syntax : heads_dic[mb][hu] = 1d-array 
        folder_forcing (str)      : path to reference forcing file that will be truncated (PESHMELBA simulation always starts at 0 even when time index go ahead). 
                                    Forcing serie should be adapted
        traj_dic (dict, OPTIONAL) : dictionnary of Trajectory instances to be updated 
        '''
        
        ### Update forcings time serie
        self.clean_TemporalSerieEnsemble()
        
        members_i = np.empty((self.stateDim,self.m))

        self.clean_DAinterfaceEnsemble() ### Make sure that no thetas are present in DA_interface file
        
        # ~ mbnone,traj_dic = self.oneStepForFullEnsemble(ti,tf,traj_dic=traj_dic,folder_forcing=folder_forcing,typesimu='init',**kwarg) ### Launch resolution
        mbnone,traj_dic = self.multStepForFullEnsemble(ti,tf,None,traj_dic=traj_dic,folder_forcing=folder_forcing,typesimu='init',**kwarg) ### Launch resolution
        theta_dic       = self.get_theta_ForFullEnsemble() ## get thetas vector values in each member repertory 
                
        for mb in range(self.m): ### Create members
            ### 1. Initialize moisture
            for ihu,hu in enumerate(self.list_HU) : ## mantain order !!
                members_i[ihu*self.PESHMELBA_mb[mb].n_layer:(ihu+1)*self.PESHMELBA_mb[mb].n_layer,mb] = theta_dic[mb][hu]
            ### 2. Initialize parameter
            if self.estim_param!=[]:
                i = (ihu+1)*self.PESHMELBA_mb[mb].n_layer
                idx_param_estimation = []
                
                for param_scal in self.estim_param:
                    idx_scal = [iprm for iprm,prm in enumerate(names) if (param_scal in prm)]
                    idx_param_estimation.append(idx_scal[0])
                
                for iparam in idx_param_estimation:
                    members_i[i,mb] = X[mb,iparam]
                    i+=1
                
        return members_i,traj_dic
        
    def oneStep_ForInst(self,ti,tf,folder_forcing,Instance,traj_dic={},cluster=False,typesimu="determ"):
        '''
        Run PESHMELBA simulation for a single extern instance 
        -- INPUT--
        ti,tf (float64)             : initial and final time for true PESH. simulation
        folder_forcing (str)        : path to reference forcing file that will be truncated (PESHMELBA simulation always starts at 0 even when time index go ahead). 
                                      Forcing serie should be adapted
        Instance (pyPESHMELBA inst) : pyPESHMELBA instance to be run
        traj_dic (dict, OPTIONAL)   : dictionnary of Trajectory instances to be updated 
        '''
        print(' Enter OneStep_ForInst')
        t=ti
        while t<tf:
            print(f'    Launch OneStep_ForInst between {t} and {t+self.dt}')
            Instance.update_forcing(t,t+self.dt,folder_forcing,self.begin_date)
            Instance.clean_TemporalSerie()
            
            if cluster:
                os.chdir(os.path.join(path_pyAD,'pyAD','models'))
                # ~ cmd      = f'./peshmelba_forecast_single.sh {Instance.path} {typesimu} {ti}'
                # ~ launch   = subprocess.run(cmd, shell=True)   
                cmd      = [f'./peshmelba_forecast_single.sh', f'{Instance.path}', f'{typesimu}', f'{ti}']         
                launch   = subprocess.call(cmd)  
        
            else:
                path_log = os.path.join(self.path,"LOGS",f"{typesimu}_t{ti}.log")
                Instance.launch()

            ### Fill trajectories in a different way (need to read ext file)
            for key,traj in traj_dic.items():
                time_traj, value_traj = traj.read_theta_fromtxt() 
                traj.append(value_traj,time_traj+t)
                                        
            t+=self.dt

        dic_theta = Instance.get_theta_npy(self.list_HU)
        
        state_vect = np.empty(self.stateDim)
        
        for ihu,hu in enumerate(self.list_HU) : ## mantain same order than in members block !!
            state_vect[ihu*Instance.n_layer:(ihu+1)*Instance.n_layer] = dic_theta[hu]
            
        return state_vect, traj_dic
        
    def check_ensemble_consistency(self,members):
        '''
        Check physical consistency of members for PESHMELBA model
        Check whether or not theta profile is consistent with residual water content. 
        ==> Values that are below thetar are set to 1.01*thetar

        -- INPUT --
        members (2d-array, float64): current state. Vector shape: (stateDim,number of members)
        
        -- OUTPUT --
        members: updated members 
        '''
        print('Enter physical consistency')
        
        for mb in range(self.m):
            
            ### Get new value of parameters after joint estimation
            dict_theta = {}
            for iparam,param in enumerate(self.estim_param):
                dict_theta[param] = members[np.shape(self.list_HU)[0]*self.PESHMELBA_mb[mb].n_layer+iparam,mb].real

            ### Correct variables
            for ihu,hu in enumerate(self.list_HU):
                
                theta_v = copy.deepcopy(members[ihu*self.PESHMELBA_mb[mb].n_layer:(ihu+1)*self.PESHMELBA_mb[mb].n_layer,mb]).real ## get theta profile for each hu 
                theta_v = self.PESHMELBA_mb[mb].check_theta_profile(hu,theta_v,dict_theta)

                members[ihu*self.PESHMELBA_mb[mb].n_layer:(ihu+1)*self.PESHMELBA_mb[mb].n_layer,mb] = theta_v

        return members
        
    def external_cluster_check(self):
        '''
        Check whether or not all members simulations ended properly
        Test content of flagend file
        
        --OUTPUT--
        simu_not_ok (1d-array,int32): list of members that did not end properly
        '''
        
        simu_not_ok = []
        
        for isimu in range(self.m):  
            pathflag = os.path.join(self.path,"MEMBERS",f"{isimu}","PESHMELBA","flagend.txt")
            try: 
                flag = os.stat(pathflag).st_size
                
            except:
                flag=0
            
            if flag==0:simu_not_ok.append(isimu)
        
        return simu_not_ok
        
    def external_write_check(self,pathname_v,target_shape,dtype='ndarray'):
        '''
        Check whether or not all members specific files have been written properly
        Try to load given files and check their size
        
        --INPUT--
        pathname_v (1d-array,str): list of pathnames for file to be checked (root directory is MEMBERX/)
        target_shape (list): targeted shape for all elements from pathname_v
        type (str): type of objects in pathname_v. Process won't be the same depending on the object type
        
        --OUTPUT--
        write_not_ok (1d-array,int32): list of members that did not write properly
        '''
        
        write_not_ok = []

        for isimu in range(self.m):  
            
            flag = 1
            
            for pathname in pathname_v:
                
                pathfull = os.path.join(self.path,"MEMBERS",f"{isimu}",pathname)
                
                if dtype=='ndarray':

                    ### 1. Check opening
                    try: 
                        with open(pathfull,'rb') as f0:
                            var_check = np.load(f0,allow_pickle=True,encoding='latin1')
                                                    
                            ### 2. Check shape
                            if np.shape(var_check)!=target_shape:
                                flag=0
                    except:
                        flag=0
                        
                elif dtype=='dict':
                    
                    try:
                        with open(pathfull,'rb') as f0:
                            var_check = np.load(f0,allow_pickle=True,encoding='latin1').item()

                        print('ok opening')

                        for key,value in var_check.items():
                            if np.shape(value)!=target_shape:
                                print('key',key,"shape is",np.shape(value),'vs target-shape',target_shape)
                                flag=0
                    except:
                        flag=0
                    
            if flag==0:write_not_ok.append(isimu)
        
        return write_not_ok

                
                        
class ObsInfo(obsinf.ObsInfo):
    
    def __init__(self, matrix=None,subx=[1,], OBS_STEP=1, R=[[1]], obs=None, time_obs=None):
        '''
        --INPUT--
        subx (scal,int32)                : vector for linear obs. operator generation. Size = stateDim ER: description a preciser 
        OBS_STEP (scal,int32)            : frequency of observation (time step unit)
        R (2d-array, float64)            : covariance matrix of observation error
        obs (2d-array, float64, OPT)     : obs serie used to initialize ObsInfo instance 
        time_obs (1d-array, float64, OPT): time indexes corresponding to initial obs serie
        '''
        super().__init__(matrix=matrix,subx=subx, OBS_STEP=OBS_STEP, R=R, obs=obs, time_obs=time_obs)
        
    def HM(self,PeshmelbaInstance,input_param,i):
        '''
        Compostition of model aplication and observation operator
        Allows for mapping input parameter space onto observation space - where state is observed
        
        --INPUT--
        PeshmelbaInstance (instance of class PESHMELBA) : model used to calculate state vector in state space. State vector are stored in ensemble_traj attribute
        input_param (1d-array, float64)                 : input parameter set for the PESHMELBA model
        i (scal, int32)                                 : indice of input parameter set. Corresponding trajectory will be taken in the ensemble_traj 2d-array at corresponding index
        
        --OUTPUT--
        y (1d-array,float64): corresponding points in observation space
        '''
        
        x = PeshmelbaInstance.ensemble_traj[:,i] ## state space
        y = np.dot(self.matrix,x) ### observation space
        
        return y
               
    def plot(self,cell,ax=None,color='black',figsize=(20,10),save=False,titlefig="observation.png",**kwarg):
        '''
        cell (int32): index of variable in observation vector to be plotted
        '''
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)
        
        ax.scatter(self.time_obs,self.obsserie[cell,:],linewidth=0.5,color=color,**kwarg)
    
        if save:    
            plt.savefig(titlefig)
               
    def step(self,ax=None,color='black',figsize=(20,10),save=False,titlefig="observation.png",**kwarg):
        '''
        '''
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)
        
        ax.step(self.time_obs,self.obsserie[:],where='post',linewidth=0.5,color=color,**kwarg)
    
        if save:    
            plt.savefig(titlefig)
        
        
    def plot_errorbar(self,cell,ref_time,ref_serie,ax=None,color='black',figsize=(20,10),save=False,titlefig="observation.png",**kwarg):
        '''
        Plot [1-99] confidence interval of observation error.
        Distribution is supposed to be gaussian
        
        -- INPUT--
        cell (int32)                 : index of variable in observation vector to be plotted. Used to select right cell in R matrix : R[cell,cell] 
        ref_serie (1d-array, float64): serie of true values observation. Error bar is centred around true value as supposed unbiased
        '''
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)
            
        ### get error
        yerr = []
        for t in ref_time:
            R    = self.Rtime[t]
            q99  = norm.ppf(0.99, loc=0, scale=np.sqrt(R[cell,cell]))
            yerr.append(q99) 
            
        print('ref_time is',ref_time)
        print('ref_serie is',ref_serie)
        print('yerr is',yerr)
        
        ax.errorbar(ref_time,ref_serie,yerr=yerr,linestyle="None",color=color,**kwarg)
    
        if save:    
            plt.savefig(titlefig)
        
        
class Trajectory(trj.Trajectory):
    '''
    Store trajectory for one state variable.
    Trajectory can be 1d (deterministic traj.) or 2d (ensemble)
    Trajectories can be filled from 1d and 2d arrays or by reading external file 
    '''
    
    def __init__(self, state_i=[], time_i=[], m=1, select_op = None, name="var",cell=0,HU=None):
        '''
        cell (int): indice of numerical cell to be plotted if theta, head,... profile
        '''
        super().__init__(state_i=state_i, time_i=time_i, m=m, select_op=select_op, name=name)
        self.pathname = {}
        self.path_moisture = {}
        self.cell = cell
        self.HU   = HU    
        
    def set_pathname(self,path,m=0):
        '''
        Set pathanme to access variables to include in trajectory
        
        --INPUT--
        path (str)      : absolute path to variable linked to trajectory
        m (int32, OPT) : if trajectory stores an ensemble, m is the member index (default is 0)
        '''
        self.pathname[m] = path
        
    def set_path_moisture_dic(self,path,m=0):
        '''
        Set path to access moisture_dic to include in trajectory
        
        --INPUT--
        path (str)      : absolute path to variable linked to trajectory
        m (int32, OPT) : if trajectory stores an ensemble, m is the member index (default is 0)
        '''
        self.path_moisture[m] = path
        
    def read_theta_fromtxt(self):
        '''
        Fills a trajectory instance from a variable read from txt file (path stored in pathname dict).
        
        --RETURN--
        time (1d-array, float64) : return time indexes corresponding to variables to be extracted
        var (2d-array, float64)  : extracted variable time serie. Can be 1d if deterministic or 2d if ensemble variable
                                   Dimension is (time serie length, number of members)
        
        '''
        
        time = []
        
        if self.m>5:
            for mb in np.arange(1,min(5,self.m)): ### try to find real time length
        
                with open(self.pathname[mb],'rb') as f:
                    data = np.loadtxt(f,skiprows=1) 
                
                time_mb = np.asarray(data[:,0]) ## col 0 is time
                
                if len(time_mb)>len(time):
                    time = copy.deepcopy(time_mb)
        else:
            with open(self.pathname[0],'rb') as f:
                data = np.loadtxt(f,skiprows=1) 
            time = np.asarray(data[:,0])

        var = np.empty((len(time),self.m))

        for mb in np.arange(0,self.m):

            with open(self.pathname[mb],'rb') as ff:
                data = np.loadtxt(ff,skiprows=1) 
            try:
                var[:,mb] = np.asarray(data[:,1+self.cell])
            except ValueError: ###duplicate existing values and fill gaps with such values
                print(f'Truncated txt file for member',mb)
                data_trunc             = copy.deepcopy(data[:,1+self.cell])
                full_vector            = np.ones(len(time))
                len_var                = len(data_trunc)
                full_vector[0:len_var] = np.asarray(data_trunc)
                
                for i in range(len(time)-len_var): ### fill trajectory
                    full_vector[i+len_var] = full_vector[len_var-1]

                var[:,mb]   = full_vector
            
        return time, var
        
    def read_theta_from_dic_npy(self):
        '''
        Fills a trajectory instance from a variable read from txt file (path stored in pathname dict).
        
        --RETURN--
        time (1d-array, float64) : return time indexes corresponding to variables to be extracted
        var (2d-array, float64)  : extracted variable time serie. Can be 1d if deterministic or 2d if ensemble variable
                                   Dimension is (time serie length, number of members)
        
        '''
        
        with open(os.path.join(self.path_moisture[0],'moisture_dic.npy'), 'rb') as f:
            data = np.load(f, allow_pickle=True, encoding='latin1').item()
        
        with open(os.path.join(self.path_moisture[0],'time_list.npy'), 'rb') as ff:
            time = np.load(ff, allow_pickle=True, encoding='latin1')

        time        = np.asarray(time)
        thetaUH     = np.asarray(data[self.HU])
        
        var      = np.empty((len(time),self.m))
        var[:,0] = np.asarray(thetaUH[:,self.cell])
        
        for mb in np.arange(1,self.m):

            with open(os.path.join(self.path_moisture[mb],'moisture_dic.npy'), 'rb') as f:
                data = np.load(f, allow_pickle=True, encoding='latin1').item()
                
            data = np.asarray(data[self.HU])            
            var[:,mb] = np.asarray(data[:,self.cell])

        return time, var
        
    def read_theta_from_dic_pickle(self):
        '''
        Fills a trajectory instance from a variable read from pickle file (path stored in pathname dict).
        
        --RETURN--
        time (1d-array, float64) : return time indexes corresponding to variables to be extracted
        var (2d-array, float64)  : extracted variable time serie. Can be 1d if deterministic or 2d if ensemble variable
                                   Dimension is (time serie length, number of members)
        
        '''
        Bool = True
        
        try:
            with open(os.path.join(self.path_moisture[0],'moisture_dic.pkl'), 'rb') as f:
                data = pickle.load(f, encoding='latin1') 
            
            with open(os.path.join(self.path_moisture[0],'time_list.npy'), 'rb') as ff:
                time = np.load(ff, allow_pickle=True, encoding='latin1')
        
        except (pickle.UnpicklingError,EOFError):
            Bool = False
            
        if Bool:
            time        = np.asarray(time)
            thetaUH     = np.asarray(data[self.HU])
            
            var      = np.empty((len(time),self.m))
            var[:,0] = np.asarray(thetaUH[:,self.cell])
            
            for mb in np.arange(1,self.m):
                
                try:
    
                    with open(os.path.join(self.path_moisture[mb],'moisture_dic.pkl'), 'rb') as f:
                        data = pickle.load(f, encoding='latin1') 
                    data = np.asarray(data[self.HU])            
                    var[:,mb] = np.asarray(data[:,self.cell])
                    
                except (pickle.UnpicklingError,EOFError,ValueError):    
                    Bool = False
                
        
        if not Bool:
            time, var = self.read_theta_fromtxt()
    
        return time, var
                
        
class Anamorphosis:
    
    def __init__(self,stateDim=1,m=1):
        '''
        Constructor
        
        -- INPUT --
        stateDim (scal,int32,OPTIONAL) : dimension of state vector for DA
        m (scal, int32)                : number of members if ensemble DA method
        '''
        
        self.stateDim = stateDim
        self.m        = m 
        
    def theta_physic2gaussian(self,z,theta_s=None,theta_r=None,alpha=None,beta=None):
        '''
        Analytical bijection from physical space to gaussian space for theta variable (PDF = truncated gaussian in physical space) 
        Bijection is the identity between alpha*theta_r and beta*theta_s and an exponential expression that tends to theta_s (resp theta_r) outside such bounds
        
        -- INPUT --
        z (scal, float64): physical moisture value to be transformed into gaussian space
        theta_s (scal, float64): moisture at saturation that stands for upper bound 
        theta_r (scal, float64): residual moisture that stands for lower bound
        alpha (scal, float64): below alpha*theta_r, the bijection is an exponential expression that -> theta_r when z -> -inf. /!\ alpha is > 1 !  
        beta (scal, float64): above beta*theta_s, the bijection is an exponential expression that -> theta_s when z -> +inf. /!\ beta is < 1 ! 
        '''
        
        if (z>alpha*theta_r) and (z<beta*theta_s):
            y = z
            
        elif (z<=alpha*theta_r): ## left tail
            fact = (alpha-1)*theta_r
            y    = alpha*theta_r + fact*np.log((z-theta_r)/fact)
            
        elif (z>=beta*theta_s): ## right tail
            fact = (1-beta)*theta_s
            y    = beta*theta_s - fact*np.log((theta_s-z)/fact)
                
        return y
        

    def theta_gaussian2physic(y,theta_s=None,theta_r=None,alpha=None,beta=None):
        """
        Analytical bijection from physical space to gaussian space for theta variable (PDF = truncated gaussian in physical space) 
        """
        
        if (y>alpha*theta_r) and (y<beta*theta_s):
            z = y
            
        elif (y<=alpha*theta_r): ## left tail
            fact = (alpha-1)*theta_r
            z    = theta_r + fact*np.exp((y-alpha*theta_r)/fact)
            
        elif (y>=beta*theta_s): ## right tail
            fact = (1-beta)*theta_s
            z    = theta_s - fact*np.exp((beta*theta_s-y)/fact)
                
        return z
        
        
    def set_moistureparam_state(self,thetar_v=None,thetas_v=None,alpha_v=None,beta_v=None):
        """
        Set hydrodynamical parameter for moisture vector transformation (state)
        
        --INPUT--
        theta_s (1d-array, float64): moisture at saturation that stands for upper bound 
        theta_r (1d-array, float64): residual moisture that stands for lower bound
        alpha (1d-array, float64): below alpha*theta_r, the bijection is an exponential expression that -> theta_r when z -> -inf. /!\ alpha is > 1 !  
        beta (1d-array, float64): above beta*theta_s, the bijection is an exponential expression that -> theta_s when z -> +inf. /!\ beta is < 1 ! 
        """
        if thetar_v is not None: self.thetar_state = thetar_v
        if thetas_v is not None: self.thetas_state = thetas_v
        if alpha_v is not None: self.alpha_state   = alpha_v
        if beta_v is not None: self.beta_state     = beta_v
        
        
    def set_moistureparam_obs(self,thetar_v=None,thetas_v=None,alpha_v=None,beta_v=None):
        """
        Set hydrodynamical parameter for moisture vector transformation (obs and obs error)
        
        --INPUT--
        theta_s (1d-array, float64): moisture at saturation that stands for upper bound 
        theta_r (1d-array, float64): residual moisture that stands for lower bound
        alpha (1d-array, float64): below alpha*theta_r, the bijection is an exponential expression that -> theta_r when z -> -inf. /!\ alpha is > 1 !  
        beta (1d-array, float64): above beta*theta_s, the bijection is an exponential expression that -> theta_s when z -> +inf. /!\ beta is < 1 ! 
        """
        if thetar_v is not None: self.thetar_obs = thetar_v
        if thetas_v is not None: self.thetas_obs = thetas_v
        if alpha_v is not None: self.alpha_obs   = alpha_v
        if beta_v is not None: self.beta_obs     = beta_v
        
    
    def anamorphosis_state_2gaussian(self,statevect):
        '''
        Perform anamorphosis (physical space to gaussian space) for each element of state vector
        
        --INPUT--
        statevect (1d-array, float64): array of elements in physical space
        '''
        
        gauss_v = np.empty(self.stateDim)
        
        for ith,theta in enumerate(statevect):
            
            gauss_v[ith] = self.theta_physic2gaussian(theta,
                                                    theta_s=self.thetas_state[ith],
                                                    theta_r=self.thetar_state[ith],
                                                    alpha=self.alpha_state[ith],
                                                    beta=self.beta_state[ith])
        
        return gauss_v
        
    
    def anamorphosis_state_2physic(self,statevect):
        '''
        Perform inverse anamorphosis (gaussian space to physical space) for each element of state vector
        
        --INPUT--
        statevect (1d-array, float64): array of elements in physical space
        '''
        
        physic_v = np.empty(self.stateDim)
        
        for ith,theta in enumerate(statevect):
            
            physic_v[ith] = self.theta_gaussian2physic(theta,
                                                    theta_s=self.thetas_state[ith],
                                                    theta_r=self.thetar_v[ith],
                                                    alpha=self.alpha_state[ith],
                                                    beta=self.beta_state[ith])
        
        return physic_v
        
    
    def anamorphosis_obs_2gaussian(self,obsvect,R):
        '''
        Perform anamorphosis (physical space to gaussian space) for each element of obs vector + observation error 
        /!\ So far, hypothetizes R is diagonal
        Transformation of obs. error is done accounting for non linearity of anamorphosis function following :
        Lien, G.-Y., Kalnay, E. et Miyoshi, T. (2013). Effective assimilation of global precipitation :
        simulation experiments. Tellus A : Dynamic Meteorology and Oceanography, 65(1):19915. 
        
        --INPUT--
        obsvect (1d-array, float64) : array of elements in physical space
        R (2d-array, float64)       : initial obs. error matrice in physical space
        
        --RETURN--
        obsvect (1d-array, float64) : array of elements in gaussian space
        R (2d-array, float64)       : transformed obs. error matrice in gaussian space
        '''
        
        gauss_v = np.empty(np.shape(obsvect))
        R_gauss = np.zeros(np.shape(R))
        
        Rphysic = np.sqrt(np.diag(R)) ## get sigma vector

        for ith,theta in enumerate(obsvect):
            
            gauss_v[ith,0] = self.theta_physic2gaussian(theta,
                                            theta_s=self.thetas_obs[ith],
                                            theta_r=self.thetar_obs[ith],
                                            alpha=self.alpha_obs[ith],
                                            beta=self.beta_obs[ith])
                                                    
            y_up  = self.theta_physic2gaussian(theta+Rphysic[ith],
                                            theta_s=self.thetas_obs[ith],
                                            theta_r=self.thetar_obs[ith],
                                            alpha=self.alpha_obs[ith],
                                            beta=self.beta_obs[ith])
                                                    
            y_dwn = self.theta_physic2gaussian(theta-Rphysic[ith],
                                            theta_s=self.thetas_obs[ith],
                                            theta_r=self.thetar_obs[ith],
                                            alpha=self.alpha_obs[ith],
                                            beta=self.beta_obs[ith])

            
            R_gauss[ith,ith] = 0.5 * (y_up + y_dwn)
            
            
                                                    
        
        
        return gauss_v, R_gauss

        
                

        
        
            
        
        

        
        
        
