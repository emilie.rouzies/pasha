import os, sys
import numpy as np
import pickle

class ObsInfo:

    def __init__(self, matrix=None,subx=None, OBS_STEP=None, R=None, obs=None, time_obs=None):
        '''
        '''

        self.OBS_STEP = OBS_STEP
        if matrix is None:
            self.matrix   = np.identity(len(subx)) * subx
            self.matrix   = self.matrix[~np.all(self.matrix == 0, axis=1)] # Remove line with 0
        else:
            self.matrix = matrix
        self.R        = R
        self.Rtime    = {}
        self.o        = np.shape(self.matrix)[0]

        if time_obs is not None:
            print('time obs not None')
            self.W        = len(time_obs)
            self.obsserie = obs
            self.time_obs = time_obs
        else:
            self.W = 0
            self.obsserie = []
            self.time_obs = []
            print('time obs None')
        
    def H(self, x) :
        '''
        '''
        return np.dot(self.matrix,x)
        
    def HM(self, ModelInstance, i) :
        '''
        '''
        raise NotImplementedError

    def H_T(self, y) :
        '''
        '''
        return np.dot(np.transpose(self.matrix),y)
        
    def set_obsserie(self,obs,time_obs):
        '''
        obs ():
        time_obs ():
        '''
        self.obsserie = obs
        self.time_obs = time_obs
        self.W        = len(time_obs)
        self.o        = np.shape(self.obsserie)[0]
        
    def set_fullRtime(self,Rscal,time):
        """
        """
        self.Rtime[time] = Rscal
        
    def get(self,t):
        '''
        '''
        idx = [i for i,tt in enumerate(self.time_obs) if tt==t]

        return self.obsserie[:,idx]
        
    def append(self,y,t):
        '''
        '''  
        

        if isinstance(t,np.ndarray):
            y = y.reshape((self.o*len(t),1))
        else:
            y = y.reshape((self.o,1))

        if self.obsserie == []:
            self.obsserie = np.array(y).reshape((self.o,len(np.reshape(t,(-1,1)))))
        else:
            self.obsserie = np.concatenate((self.obsserie,y),axis=1)
            
        self.time_obs = np.append(self.time_obs,t)
        self.W        = len(self.time_obs)

    def noisy(self,true,R=None):
        '''
        Perturbs a given input vector applying noise defined accordingly to obs error covariance matrix
        
        --INPUT--
        true (1d-array, float64)  : true observation vector, length = self.o
        R (2d-array, float64, OPT): matrix of covariance of obs. error
        
        -- RETURN--
        pertur (1d-array, float64): perturbed observation vector, length = self.o
        '''
        
        if R is None:
            R = self.R
        ## 1. Generate gaussian white noise, covariance = Id 
        whnoise = np.random.multivariate_normal(np.zeros(self.o),np.eye(self.o))
        
        ## 2. Transform sample to match obs error cov. matrix description
        cholR = np.linalg.cholesky(R)
        noise = np.dot(cholR,whnoise)
        
        return true + noise

    def noisy_test(self,true,R=None):
        '''
        Perturbs a given input vector applying noise defined accordingly to obs error covariance matrix
        
        --INPUT--
        true (1d-array, float64)  : true observation vector, length = self.o
        R (2d-array, float64, OPT): matrix of covariance of obs. error
        
        -- RETURN--
        pertur (1d-array, float64): perturbed observation vector, length = self.o
        '''
        noisy = np.empty(self.o)
                
        if R is None:
            R = self.R
        
        for ir in range(self.o):
            std = np.random.normal(0,np.sqrt(R[ir,ir]))
            noisy = true[ir] + std 
        
        return noisy
        
    def plot(self,**kwarg):
        raise NotImplementedError
        
    def step(self,**kwarg):
        raise NotImplementedError
        
    def plot_errorbar(self,**kwarg):
        raise NotImplementedError
        
    def save(self,directory,name):
        '''
        '''
        dict_obs  = {"time":self.time_obs,"var":self.obsserie}
        full_path = os.path.join(directory,f"{name}.npy")
        np.save(full_path,dict_obs)

        
    def save_asdic(self,directory,name):
        '''
        Save only time serie and value serie as a dictionnary
        
        --INPUT--
        directory (str): path for saving
        name (str)     : name for saved file without extension 
        '''
        
        dict_obs  = {"time":self.time_obs,"var":self.obsserie}
        full_path = os.path.join(directory,f"{name}.npy")
        np.save(full_path,dict_obs)
        
    def save_aspickle(self,directory,name):
        '''
        Save ObsInfo fullinstance as a pickle format
        
        --INPUT--
        directory (str): path for saving
        name (str)     : name for saved file without extension 
        '''
        full_path = os.path.join(directory,f"{name}.pickle")
        with open(full_path, 'wb') as filem: 
            pickle.dump(self, filem)        
