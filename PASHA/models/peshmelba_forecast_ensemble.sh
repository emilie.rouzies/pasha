#!/bin/bash    

### input 1 is number of members
### input 2 is directory where all members are stored
### input 3 is type of ensemble simulation (forecast, OL)
### input 4 is a reference time for saving logs

JOBIDF=$(sbatch --parsable --array=0-$(($1-1))%50 ./peshmelba_launch_ensemble_cluster.sh $2 $3 $4)

echo "${JOBIDF} ensemble forecast job"
until (($(scontrol show job $JOBIDF | grep -c JobState=COMPLETED)==$1))
do
   sleep 5 
   echo "Forecast still on going"
done

echo "Forecast step over"
