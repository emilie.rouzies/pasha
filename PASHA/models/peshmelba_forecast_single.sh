#!/bin/bash    

### input 1 is directory
### input 2 is type of ensemble simulation (true...)
### input 3 is a reference time for saving logs

JOBIDF=$(sbatch --parsable --array=0-0%50 ./peshmelba_launch_single_cluster.sh $1 $2 $3)

echo "${JOBIDF} single forecast job"
until (($(scontrol show job $JOBIDF | grep -c JobState=COMPLETED)==1))
do
   sleep 5 
   echo "Forecast still on going"
done

echo "Forecast step over"
