#!/usr/bin/python
# -*- coding: utf-8 -*-

import os,sys
import numpy as np

path_PASHA = 'path/to/packagePASHA'
sys.path.append(path_PASHA)

import PASHA.models.model_instance as mi
import PASHA.models.obs_info as obsinf

class AnharmonicOscillatorJoint(mi.ModelInstance):
    
    def __init__(self, dt=1, stateDim=4):
        '''
        '''
        super().__init__(dt=dt, stateDim=stateDim)
        
    def oneStep(self,ti,tf,state,traj_dic={}):
        '''
        Propagates state for one time step
        
        -- INPUT --
        state (1d-array, float64): current state. Vector of shape stateDim
        
        -- RETURN--
        upd (1d-array, float64): updated state. Vector of shape stateDim
        '''
        W     = state[2]
        L     = state[3]
        
        alpha = 2 + W * W - L * L * state[0] * state[0]
        M     = [[alpha, -1, 0, 0],[1, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]
        upd   = np.matmul(M,state)
        
        return upd,traj_dic
        
class ObsInfo(obsinf.ObsInfo):
    
    def __init__(self, subx=[1,0,0,0], OBS_STEP=1, R=[[1]], obs=None, time_obs=None):
        '''
        --INPUT--
        subx (scal,int32):
        OBS_STEP (scal,int32): frequency of observation (time step unit)
        '''
        super().__init__(subx=subx, OBS_STEP=OBS_STEP, R=R, obs=obs, time_obs=time_obs)
        
    def set_obsserie(self,obs,time_obs):
        '''
        --INPUT--
        obs (1d-array,float64):time serie of observations
        '''
        
        self.obsserie = np.reshape(obs,(1,len(time_obs))) ## Check format
        self.time_obs = time_obs 
        self.W        = len(time_obs)
        self.o        = 1
                
    def plot(self,ax=None,figsize=(20,10),save=False,titlefig="observation.png",**kwarg):
        '''
        '''
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)
        
        ax.scatter(self.time_obs,self.obsserie,linewidth=0.5,**kwarg)
    
        if save:    
            plt.savefig(titlefig)
        
        
        
