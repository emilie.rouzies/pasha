#!/bin/bash
#SBATCH --job-name=determPESHMELBA

## Launch PESHMELBA
DETERLAUNCH=${1}/PESHMELBA/
cd $DETERLAUNCH
source /nfs/home/peshmelba/chantilly/bin/activate
module load mpich/ge/gcc/64/3.2
./run_mpi1.sh
deactivate 
    

