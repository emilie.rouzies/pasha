#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import numpy as np

path_PASHA = 'path/to/packagePASHA'
sys.path.append(path_PASHA)

import PASHA.models.model_instance as mi
import PASHA.models.obs_info as obsinf

class Lorenz(mi.ModelInstance):
    
    def __init__(self, sigma=10.0, beta=8./3, rho=28.0, dt=0.001, stateDim=3):
        '''
        
        sigma (scal, float64): sigma parameter
        beta (scal, float64): beta parameter
        rho (scal, float64): rho parameter
        '''
        
        super().__init__(dt=dt, stateDim=stateDim)

        self.sigma = sigma
        self.beta  = beta
        self.rho   = rho
        
    def oneStep(self,ti,tf,state,traj_dic={}):
        '''
        Propagates state for one time step
        
        -- INPUT --
        state (1d-array, float64): current state. Vector of shape stateDim
        
        -- RETURN--
        upd (1d-array, float64): updated state. Vector of shape stateDim
        '''
        
        state = np.real(state)
        res   = np.zeros(self.stateDim)
        
        res[0] = state[0] + self.dt * self.sigma * (state[1] - state[0])
        res[1] = state[1] + self.dt * (self.rho*state[0] - state[1] - state[0]*state[2])
        res[2] = state[2] + self.dt * (-self.beta*state[2] + state[0]*state[1])

        return res, traj_dic

class ObsInfo(obsinf.ObsInfo):
    
    def __init__(self, subx=[1,1,1], OBS_STEP=1, R=[[1]], obs=None, time_obs=None):
        '''
        --INPUT--
        subx (scal,int32):
        OBS_STEP (scal,int32): frequency of observation (time step unit)
        '''
        super().__init__(subx=subx, OBS_STEP=OBS_STEP, R=R, obs=obs, time_obs=time_obs)
        
    def set_obsserie(self,obs,time_obs):
        '''
        --INPUT--
        obs (1d-array,float64):time serie of observations
        '''
        
        self.obsserie = np.reshape(obs,(3,len(time_obs))) ## Check format
        self.time_obs = time_obs 
        self.W        = len(time_obs)
        self.o        = 3

    def plot_x(self,ax=None,figsize=(20,10),save=False,titlefig="observation.png",**kwarg):
        '''
        '''
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)
        
        ax.scatter(self.time_obs,self.obsserie[0,:],linewidth=0.5,**kwarg)
    
        if save:    
            plt.savefig(titlefig)
            
    def plot_y(self,ax=None,figsize=(20,10),save=False,titlefig="observation.png",**kwarg):
        '''
        '''
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)
        
        ax.scatter(self.time_obs,self.obsserie[1,:],linewidth=0.5,**kwarg)
    
        if save:    
            plt.savefig(titlefig)
