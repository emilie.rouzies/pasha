#!/bin/bash
#SBATCH --job-name=forecast_PESHMELBA


## Launch PESHMELBA
echo ${SLURM_JOBID}
MEMBERLAUNCH=${1}/${SLURM_ARRAY_TASK_ID}/PESHMELBA/
EX_DIR=${1}/${SLURM_ARRAY_TASK_ID}
echo $MEMBERLAUNCH
echo ${2}
echo ${3}
cd $MEMBERLAUNCH

BASE_DIR=/nfs/projets/peshmelba

#~ ### Step 1: set config_param files
#~ echo "$EX_DIR/" > $EX_DIR/PESHMELBA/config_param.txt
#~ echo  "folder=\"$EX_DIR\"" > $EX_DIR/PESHMELBA/config_param.py

#~ ### Step 2: copy Make.include 
#~ cp $BASE_DIR/Make.include_mpi1 $EX_DIR/PESHMELBA/Make.include

#~ ### Step 2: copy Make.include 
#~ cp $BASE_DIR/Make.include_mpi1 $EX_DIR/PESHMELBA/Make.include

#~ ### Step 3: change python path
#~ cd $EX_DIR/PESHMELBA

#~ for filename in main_*.py; do
#~ done

#~ cd ROSS03
#~ make clean
#~ cd ..
#~ make clean    echo filename
    #~ sed -i '1d' $filename 
    #~ cat $BASE_DIR/pythonpath $filename > buffer
    #~ mv buffer $filename
#~ done

#~ cd ROSS03
#~ make clean
#~ cd ..
#~ make clean

source /nfs/projets/peshmelba/chantilly/bin/activate
module load mpich/ge/gcc/64/3.2 gcc/5.2.0
make
./run_mpi1.sh 
deactivate 
    
if grep "flag ok" flagend.txt
then
    echo "OK simu ended properly"
else
    echo "/!\ Simu has not ended properly"
fi
