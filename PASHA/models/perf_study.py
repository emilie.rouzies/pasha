import numpy as np
from sklearn.metrics import mean_squared_error

def crps_hersbach_decomposition(ens, obs):
    """
    Adapted from package ensverif: https://pypi.org/project/ensverif/#description
    #Konstantin Ntokas
    #
    #
    # 2019-11-17
    # -----------------------------------------------------------------------------
    # This function decomposes the CRPS into reliability and "potential" 
    # components according to Hersbach (2000). The potential CRPS
    # represents the best possible CRPS value that could be achieved, if the forecasts
    # were perfectly reliable. The total (CRPS_tot) is the empirical CRPS according to the definition of
    # Hersbach (2000)
    #
    # Hersbach, H., 2000. Decomposition of the continuous ranked probability score for ensemble 
    # prediction systems. Weather Forecast. 15, 550–570.
    #
    # inputs: 
    #           ens:    mxn matrix; m = number of records (validity dates)  
    #                                       n = number of members in ensemble 
    #           obs:    mx1 vector; m = number of records (validity dates, matching the ens) 
    #
    # outputs:   
    #           CRPS_tot:       Scalar -> reliability + potential 
    #           reliability:    Scalar -> the reliability component of the CRPS
    #           potential:      Scalar -> the potential CRPS that would be reached for a perfectly
    #                            reliable system
    # -----------------------------------------------------------------------------
    """
    # preparation 
    ens = np.array(ens, dtype='float64')
    obs = np.array(obs, dtype='float64')
    dim1 = ens.shape
    if len(dim1) == 1: 
        ens = ens.reshape((1,dim1[0]))
    dim2 = obs.shape
    if len(dim2) == 0: 
        obs = obs.reshape((1,1))
    elif len(dim2) == 1:
        obs = obs.reshape((dim2[0],1))

    m, n = ens.shape
    alpha = np.zeros((m,n+1))
    beta = np.zeros((m,n+1))
    
    for i in range(m):
        # if observation does not exist, no ens for alpha and beta
        if ~np.isnan(obs[i]):
            ensemble_sort = np.sort(ens[i]);
            for k in range(n+1):
                if k == 0:
                    if obs[i] < ensemble_sort[0]:
                        alpha[i,k] = 0
                        beta[i,k] = ensemble_sort[0] - obs[i]
                    else:
                        alpha[i,k] = 0
                        beta[i,k] = 0
                elif k == n:
                    if obs[i] > ensemble_sort[n-1]:
                        alpha[i,k] = obs[i] - ensemble_sort[n-1]
                        beta[i,k] = 0
                    else:
                        alpha[i,k] = 0
                        beta[i,k] = 0
                else:
                    if obs[i] > ensemble_sort[k]:
                        alpha[i,k] = ensemble_sort[k] - ensemble_sort[k-1]
                        beta[i,k] = 0
                    elif obs[i] < ensemble_sort[k-1]:
                        alpha[i,k] = 0
                        beta[i,k] = ensemble_sort[k] - ensemble_sort[k-1]
                    elif (obs[i] >= ensemble_sort[k-1]) and (obs[i] <= ensemble_sort[k]):
                        alpha[i,k] = obs[i] - ensemble_sort[k-1]
                        beta[i,k] = ensemble_sort[k] - obs[i]
                    else:
                        alpha[i,k] = np.nan
                        beta[i,k] = np.nan
        else: 
            alpha[i,:] = np.nan
            beta[i,:] = np.nan
            
            
    alpha1 = np.nanmean(alpha, axis=0)
    beta1 = np.nanmean(beta, axis=0)
    
    g = alpha1 + beta1  
    o = beta1 / g
    
    weight = np.arange(n+1) / n 
    reliability = np.nansum(g * np.power(o - weight, 2))
    potential = np.nansum(g * o * (1 - o))
    CRPS_tot = reliability + potential
                    
    return CRPS_tot, reliability, potential

class PerfStudy:
    
    def __init__(self, Points):
        '''
        Constructor
        --INPUT--
        Points (2d-array, float64): model values, ensemble or deterministic (can be Trajectory class instance if temporal)
        Ref: Trajectory class instance: reference
        select_op (1d-array, float64) : base vector for building transfer matrix from Ref space to Point space  
        '''
        
        self.Pts       = Points
        self.m         = self.Pts.m
        
        self.RMSE_traj = []
        self.RMSE_mean = 0
        CRPS_tot       = 0 
        reliability    = 0
        potential      = 0
        
    def set_time_perf(self,time):
        '''
        Get time indexes list for performance computation.
        They are time indexes on which both trajectory value and observation are available
        '''
        self.time_perf = sorted(list(set(self.Pts.time).intersection(time)))
                           
    def RMSE(self,Ref):
        '''
        Computes Root Mean Square Error between points from trajectory and a reference trajectory
        
        -- INPUT --
        Ref (instance of Trajectory class): reference trajectory on which points must be compared
        
        -- RETURN --
        RMSE_traj (1d-array, float64) : RMSE on each point where ref is available
        RMSE_mean (scal, float64): mean RMSE over all ref points 
        '''
        
        self.set_time_perf(Ref.time) ### Update available time for perf. assessment
        pts       = self.Pts.traj.reshape((len(self.Pts.time),self.m))
        ref_traj  = Ref.traj.reshape((len(Ref.time),Ref.m))
        RMSE_traj = []
        
        if self.time_perf :
            for t in self.time_perf:
                RMSE_t = 0
                ref_t  = np.mean(ref_traj[list(Ref.time).index(t),:])

                for mb in range(self.m):                    
                    RMSE_t+= (pts[list(self.Pts.time).index(t),mb] - ref_t)**2

                RMSE_traj.append(np.sqrt(RMSE_t) / self.m)
                
            self.RMSE_traj = RMSE_traj
            RMSE_mean = np.mean(self.RMSE_traj)
        
        
        return RMSE_traj, RMSE_mean

    def EEP(self,Ref):
        '''
        Compute Ensemble Error (EE) and ensemble Precision (EP) following 
        Bailey, R. T. & Bau, D., "Estimating geostatistical parameters and spatially-variable hydraulic conductivity within a catchment system using an ensemble smoother"
        Hydrology and Earth System Sciences, 2012, 16, 287-304, DOI: 10.5194/hess-16-287-2012
        '''
        
        self.set_time_perf(Ref.time) ### Update available time for perf. assessment
        pts       = self.Pts.traj.reshape((len(self.Pts.time),self.m))
        ref_traj  = Ref.traj.reshape((len(Ref.time),Ref.m))
        
        EE = []
        EP = []
        
        if self.time_perf :
            for t in self.time_perf:
                print('t is',t)
                ref_t  = np.mean(ref_traj[list(Ref.time).index(t),:]) ## true
                EE_scl = abs(np.mean(pts[list(self.Pts.time).index(t),:])-ref_t) ### |mean(X)-Xtrue|
                
                print('little test')
                print(np.mean(pts[list(self.Pts.time).index(t),:]),ref_t)
                
                EP_scl = np.sum([abs(np.mean(pts[list(self.Pts.time).index(t),:])-pts[list(self.Pts.time).index(t),mb]) for mb in range(self.Pts.m)])/self.Pts.m
                EE.append(EE_scl) 
                EP.append(EP_scl) 
                        
        return EE,EP
        
    def EEP_overall(self,Ref):
        '''
        Compute mean Ensemble Error (EE) and ensemble Precision (EP) following 
        Bailey, R. T. & Bau, D., "Estimating geostatistical parameters and spatially-variable hydraulic conductivity within a catchment system using an ensemble smoother"
        Hydrology and Earth System Sciences, 2012, 16, 287-304, DOI: 10.5194/hess-16-287-2012
        '''
        
        self.set_time_perf(Ref.time) ### Update available time for perf. assessment
        pts       = self.Pts.traj.reshape((len(self.Pts.time),self.m))
        ref_traj  = Ref.traj.reshape((len(Ref.time),Ref.m))
        
        EE = []
        EP = []
        
        if self.time_perf :
            for t in self.time_perf:
                print('t is',t)
                ref_t  = np.mean(ref_traj[list(Ref.time).index(t),:]) ## true
                EE_scl = abs(np.mean(pts[list(self.Pts.time).index(t),:])-ref_t) ### |mean(X)-Xtrue|
                
                print('little test')
                print(np.mean(pts[list(self.Pts.time).index(t),:]),ref_t)
                
                EP_scl = np.sum([abs(np.mean(pts[list(self.Pts.time).index(t),:])-pts[list(self.Pts.time).index(t),mb]) for mb in range(self.Pts.m)])/self.Pts.m
                EE.append(EE_scl) 
                EP.append(EP_scl) 
                        
        return np.mean(EE),np.mean(EP)
        
    def CRPS_overall(self, Ref):
        '''
        Computes Continuous Ranked Probability Score (Brown, 1974, Hersbach, 200) over the whole observation points
        Code from Konstantin Ntokas is used to compute such decomposition
        Source code available at: https://pypi.org/project/ensverif/#description
        
        -- INPUT --
        Ref (instance of Trajectory class): reference trajectory on which points must be compared
        
        -- RETURN --
        CRPS_tot (scal, float64) : reliability + potential CRPS
        reliability (scal, float64): reliability component of CRPS 
        potential (scal, float64): potential CRPS: CRPS that would be calculated if the ensemble was fully reliable
        '''
        
        self.set_time_perf(Ref.time) ### Update available time for perf. assessment
        pts_tidx  = [list(self.Pts.time).index(t) for t in self.time_perf]
        ens       = self.Pts.traj[pts_tidx,:].reshape((len(self.time_perf),self.m)) ### Select only common points for ref-traj comparison
        ref_tidx  = [list(Ref.time).index(t) for t in self.time_perf]
        ref       = np.mean(Ref.traj.reshape((len(Ref.time),Ref.m))[ref_tidx,:], axis = 1).reshape((len(self.time_perf),1)) ### Select only common points for ref-traj comparison
        
        CRPS_tot, reliability, potential = crps_hersbach_decomposition(ens, ref)
        
        return CRPS_tot, reliability, potential
        
    def CRPS_traj(self, Ref):
        '''
        Computes Continuous Ranked Probability Score (Brown, 1974, Hersbach, 200) trajectory
        Code from Konstantin Ntokas is used to compute such decomposition
        Source code available at: https://pypi.org/project/ensverif/#description
        
        -- INPUT --
        Ref (instance of Trajectory class): reference trajectory on which points must be compared
        
        -- RETURN --
        CRPS_tot_traj (1d-array, float64) : reliability + potential CRPS
        reliability_traj (1d-array, float64): reliability component of CRPS 
        potential_traj (1d-array, float64): potential CRPS: CRPS that would be calculated if the ensemble was fully reliable
        '''
        
        print(np.shape(Ref.traj))
        
        self.set_time_perf(Ref.time) ### Update available time for perf. assessment
        pts_tidx  = [list(self.Pts.time).index(t) for t in self.time_perf]
        ens       = self.Pts.traj[pts_tidx,:].reshape((len(self.time_perf),self.m)) ### Select only common points for ref-traj comparison
        ref_tidx  = [list(Ref.time).index(t) for t in self.time_perf]
        ref       = np.mean(Ref.traj.reshape((len(Ref.time),Ref.m))[ref_tidx,:], axis = 1).reshape((len(self.time_perf),1)) ### Select only common points for ref-traj comparison

        CRPS_tot_traj    = []
        reliability_traj = []
        potential_traj   = []
        
        for it,t in enumerate(self.time_perf):
            CRPS_tot, reliability, potential = crps_hersbach_decomposition([ens[it,:]], [ref[it,:]])
            CRPS_tot_traj.append(CRPS_tot)
            reliability_traj.append(reliability)
            potential_traj.append(potential)
            
        return CRPS_tot_traj, reliability_traj, potential_traj
        
    def plot_RMSE_traj(self,Ref,ax=None,figsize=(20,10),color="black",linestyle="-",linewidth=1,save=False,titlefig="RMSE_traj",**kwarg):
        '''
        Plots Root Mean Square Error between points from trajectory and a reference trajectory
        '''
        RMSE_traj, RMSE_mean = self.RMSE(Ref)
        
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)

        ax.plot(self.time_perf,RMSE_traj,color=color,linestyle=linestyle,linewidth=linewidth,**kwarg)

        if save:    
            plt.savefig(titlefig+"_"+self.name+".png")
            
         
    def plot_CRPS_traj(self,Ref,ax=None,figsize=(20,10),color="black",linestyle="-",linewidth=1,save=False,titlefig="CRPS_traj",label=""):
        '''
        Plots Continuous Ranked Probability Score (Brown, 1974, Hersbach, 200) trajectory
        '''
        CRPS_tot_traj, reliability_traj, potential_traj = self.CRPS_traj(Ref)
        
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)

        ax.plot(self.time_perf,CRPS_tot_traj,label="CRPS total - " + label,color=color,linestyle='solid',linewidth=linewidth)
        ax.plot(self.time_perf,reliability_traj,label="CRPS reliability - " + label,color=color,linestyle='dashed',linewidth=linewidth)
        ax.plot(self.time_perf,potential_traj,label="CRPS potential - " + label,color=color,linestyle='dotted',linewidth=linewidth)

        if save:    
            plt.savefig(titlefig+"_"+self.name+".png")    
               
         
    def plot_EEP_traj(self,Ref,ax=None,figsize=(20,10),color="black",linestyle="-",linewidth=1,save=False,titlefig="CRPS_traj",label=""):
        '''
        Plots ensemble error and ensemble precision trajectories
        '''
        EE, EP = self.EEP(Ref)
        
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)

        ax.plot(self.time_perf,EE,label="EE - " + label,color=color,linestyle='solid',linewidth=linewidth)
        ax.plot(self.time_perf,EP,label="EP - " + label,color=color,linestyle='dashed',linewidth=linewidth)


        if save:    
            plt.savefig(titlefig+"_"+self.name+".png")       
        
        
        
        
