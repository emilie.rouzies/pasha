# -*- coding: utf-8 -*-
import numpy as np

class ModelInstance:
    
    def __init__(self, dt=1, stateDim=1, m=1):
        '''
        Initializes instance of Model
        '''
        
        self.stateDim = stateDim
        self.dt       = dt
        self.m        = m
        
    def oneStep(self,ti,tf,state,traj_dic={}):
        '''
        Propagates model for one time step
        
        -- INPUT --
        ti,tf (scal, float64): initial and final times ER: not really useful because tf-ti = dt but needed for genericity...
        state (1d-array, float64): current state. Vector of shape stateDim
        traj_dic (dict,OPTIONAL): ensemble of trajectories to update
        '''
        raise NotImplementedError

    def multSteps(self,ti,tf,state,traj_dic={}):
        '''
        Propagates model for several time steps
        
        -- INPUT --
        ti,tf (scal, float64): initial and final times 
        state (1d-array, float64): current state. Vector of shape stateDim
        traj_dic (dict,OPTIONAL): ensemble of trajectories to update
        
        -- RETURN--
        upd (1d-array, float64): updated state. Vector of shape stateDim
        traj_dic (dict,OPTIONAL): ensemble of trajectories to update
        '''
        
        upd = state
        t   = ti
        while t<tf:
            upd = self.oneStep(ti,tf,upd,traj_dic=traj_dic)
            t+=self.dt
            for key,traj in traj_dic.items(): 
                traj.append(traj.pick(upd),t) 
                           
        return upd, traj_dic
        
    def oneStepForFullEnsemble(self,ti,tf,ensemble,traj_dic={},cluster=False,**kwarg):
        '''
        Propagates model for one time step in case of ensemble forecast

        -- INPUT --
        ti,tf (scal, float64): initial and final times ER: not really useful because tf-ti = dt but needed for genericity...
        ensemble (2d-array, float64): current state. Vector shape: (stateDim,number of members)
        traj_dic (dict,OPTIONAL): ensemble of trajectories to update
        
        -- RETURN--
        upd_ensemble (2d-array, float64): updated state. Vector shape: (stateDim,number of members)
        traj_dic (dict,OPTIONAL): ensemble of trajectories to update
        '''
        upd_ensemble = np.zeros((np.shape(ensemble)[0], np.shape(ensemble)[1]))
        
        for i in range(np.shape(ensemble)[1]):
            upd_ensemble[:, i],trj = self.oneStep(ti,tf,ensemble[:, i])
            upd_ensemble[:, i]     = upd_ensemble[:, i].ravel()
        
        for key,traj in traj_dic.items(): 
            traj.append(traj.pick(upd_ensemble),tf) 
        
        return upd_ensemble, traj_dic
        
    def multStepForFullEnsemble(self,ti,tf,ensemble,traj_dic={},cluster=False,path_log='./',**kwarg):
        '''
        Propagates model for several time step in case of ensemble forecast

        -- INPUT --
        ti,tf (scal, float64): initial and final times 
        ensemble (2d-array, float64): current state. Vector shape: (stateDim,number of members)
        traj_dic (dict,OPTIONAL): ensemble of trajectories to update
        
        -- RETURN--
        upd_ensemble (2d-array, float64): updated state. Vector shape: (stateDim,number of members)
        traj_dic (dict,OPTIONAL): ensemble of trajectories to update
        '''
        
        upd_ensemble = ensemble
        
        t=ti
        while t<tf:
            upd_ensemble, traj_dic = self.oneStepForFullEnsemble(t,t+self.dt,upd_ensemble,traj_dic=traj_dic,cluster=cluster,path_log='./',**kwarg)
            t+=self.dt
        
        return upd_ensemble, traj_dic
        
    def check_ensemble_consistency(self,members):
        '''
        Check physical consistency of members.
        Return corrected members
        
        
        -- INPUT --
        members (2d-array, float64): current state. Vector shape: (stateDim,number of members)
        
        -- OUTPUT --
        members: updated members after physical consistency check
        '''
        
        return members
