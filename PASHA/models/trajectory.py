import os,sys
import numpy as np
import matplotlib.pyplot as plt

class Trajectory:
    '''
    Store trajectory for one state variable.
    Trajectory can be 1d (deterministic traj.) or 2d (ensemble) 
    '''
    
    def __init__(self, state_i=[], time_i=[], m=1, select_op = None, name="var"):
        '''
        Trajectory can be initialized from scratch of from existing array
         
        --INPUT--
        state_i (nD-array,float64) : value(s) for trajectory initialization. Scalar if deterministic traj. is initialized from scratch,
                                                                             1D array (length,1) if determinitic traj. is initialized from exisiting traj. 
                                                                             1D array (1,nb members) if ensemble traj is initialized from scratch
                                                                             2D array (length,nb members) if ensemble is initialized from exisiting traj.
        time_i (1D-array,float64) : time for trajectory initialization
        m (scal, int32): number of members
        '''
        self.name = name
        
        if state_i==[]: ##empty, need initializing other way
            self.traj = np.empty((0,m)) 
        else:
            if np.shape(state_i)==(): ### Scalar trajectory
                self.traj = np.atleast_1d(state_i)
            else: ### Ensemble trajectory
                self.traj = state_i

        if np.shape(time_i)==(): ### Scalar trajectory 
            self.time  = np.atleast_1d(time_i)
        else:
            self.time  = time_i

        self.length    = len(self.time)
        self.m         = m 
        self.select_op = select_op 
                
                                
    def get(self,t):
        '''
        '''
        idx = np.where(self.time == t)
        
        if self.m ==1:
            return self.traj[idx]
        else:
            return self.traj[idx,:]
            
        
    def append(self,state,time):
        '''
        '''

        if self.m==1: ##1D trajectory
            self.traj = np.append(self.traj,state)
        else: ##ensemble trajectory   
            self.traj = np.vstack((self.traj,state))
        
        self.time   = np.append(self.time,time)
        self.length = len(self.time)
        
    def pick(self,x):
        '''
        '''
        return np.dot(self.select_op,x)
        
        
    def plot(self,xdata=None,ax=None,figsize=(20,10),color="black",linestyle="-",linewidth=1,save=False,titlefig="trajectory",label=None,**kwarg):
        '''
        xdata (Trajectory instance) 
        '''
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)
        
        if label is None:
            label=self.name
                
        if self.m ==1: ### plot one trajectory
            if xdata==None:
                xdata = self.time
            else:
                if xdata.m == self.m:
                    xdata = xdata.traj
                else:
                    print('different ensemble sizes for plotting')
            ax.plot(xdata,self.traj,label=label,color=color,linestyle=linestyle,linewidth=linewidth)
        
        else: ### plot ensemble of trajectory
            
            if xdata is None:
                xdata = self.time
            else:
                if xdata.m == self.m:
                    xdata = xdata.traj
                    
            for j in range(self.m):
                ax.plot(xdata,self.traj[:,j],color=color,linewidth=0.5,alpha=0.2)
            fill_btw_up  = [max(self.traj[it,:]) for it,t in enumerate(xdata)]
            fill_btw_dwn = [min(self.traj[it,:]) for it,t in enumerate(xdata)]
            ax.fill_between(xdata,fill_btw_dwn,y2=fill_btw_up,color=color,alpha=0.2)
            
        ax.set_xlim([min(xdata),max(xdata)])
            
        
        if save:    
            plt.savefig(titlefig+"_"+self.name+".png")
        
    def step(self,xdata=None,ax=None,figsize=(20,10),color="black",linestyle="-",linewidth=1,save=False,titlefig="trajectory",label=None,**kwarg):
        '''
        xdata (Trajectory instance) 
        '''
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)
        
        if label is None:
            label=self.name
                
        if self.m ==1: ### plot one trajectory
            if xdata==None:
                xdata = self.time
            else:
                if xdata.m == self.m:
                    xdata = xdata.traj
                else:
                    print('different ensemble sizes for plotting')
            ax.step(xdata,self.traj,label=label,color=color,linestyle=linestyle,linewidth=linewidth)
        
        else: ### plot ensemble of trajectory
            
            if xdata==None:
                xdata = self.time
            else:
                if xdata.m == self.m:
                    xdata = xdata.traj
                    
            for j in range(self.m):
                ax.step(xdata,self.traj[:,j],color=color,linewidth=0.5,alpha=0.2)
            fill_btw_up  = [max(self.traj[it,:]) for it,t in enumerate(xdata)]
            fill_btw_dwn = [min(self.traj[it,:]) for it,t in enumerate(xdata)]
            ax.fill_between(xdata,fill_btw_dwn,y2=fill_btw_up,color=color,alpha=0.2)
            
        ax.set_xlim([min(xdata),max(xdata)])
            
        
        if save:    
            plt.savefig(titlefig+"_"+self.name+".png")
            
    def plot_mean(self,xdata=None,ax=None,figsize=(20,10),color="black",linestyle="-",linewidth=1,save=False,titlefig="trajectory_mean",label=None,**kwarg):
        '''
        '''
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)
        if xdata is None:
            xdata = self.time
        else:
            if xdata.m ==1:
                xdata = xdata.traj
            else:
                xdata = np.mean(xdata.traj,axis=1)
        
        if label is None:
            label = "Mean - "+self.name
            
        if self.m ==1:
            ax.plot(xdata,self.traj,label=label,color=color,linestyle=linestyle,linewidth=linewidth,**kwarg)
        else:
            ax.plot(xdata,np.mean(self.traj,axis=1),label=label,color=color,linestyle=linestyle,linewidth=linewidth,**kwarg)
            
        ax.set_xlim([min(xdata),max(xdata)])
        
        if save:    
            plt.savefig(titlefig+"_"+self.name+".png")
            
    def plot_step(self,xdata=None,ax=None,figsize=(20,10),color="black",linestyle="-",linewidth=1,save=False,titlefig="trajectory",label=None):
        '''
        xdata (Trajectory instance) 
        '''
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)
            
        if label is None:
            label=self.name        
        
        if self.m ==1: ### plot one trajectory
            if xdata==None:
                xdata = self.time
            else:
                if xdata.m == self.m:
                    xdata = xdata.traj
                else:
                    print('different ensemble sizes for plotting')
            ax.step(xdata,self.traj,where="post",label=label,color=color,linestyle=linestyle,linewidth=linewidth)
        
        else: ### plot ensemble of trajectory
            if xdata==None:
                xdata = self.time                
            else:
                if xdata.m == self.m:
                    xdata = xdata.traj
                    
            fill_btw_up  = [max(self.traj[it,:]) for it,t in enumerate(xdata)]
            fill_btw_dwn = [min(self.traj[it,:]) for it,t in enumerate(xdata)]
            ax.fill_between(xdata,fill_btw_dwn,y2=fill_btw_up,step='post',color=color,alpha=0.2)
            
            for j in range(self.m):
                ax.step(xdata,self.traj[:,j],where="post",label=label,color=color,linestyle=linestyle,linewidth=0.1,alpha=0.3)
            
            ax.set_xlim([min(xdata),max(xdata)])
        
        if save:    
            plt.savefig(titlefig+"_"+self.name+".png")
            
    def plot_step_mean(self,xdata=None,ax=None,figsize=(20,10),color="black",linestyle="-",linewidth=1,save=False,titlefig="trajectory_mean",label=None):
        '''
        '''
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)
        if xdata==None:
            xdata = self.time
        else:
            if xdata.m ==1:
                xdata = xdata.traj
            else:
                xdata = np.mean(xdata.traj,axis=1)
                
        if label is None:
            label="Mean - "+self.name
        
        if self.m ==1:
            ax.step(xdata,self.traj,where="post",label=label,color=color,linestyle=linestyle,linewidth=linewidth)
        else:
            ax.step(xdata,np.mean(self.traj,axis=1),where="post",label=label,color=color,linestyle=linestyle,linewidth=linewidth)
        
        if save:    
            plt.savefig(titlefig+"_"+self.name+".png")
            
    def plot_distrib(self,time,kde=False,ax=None,color="black",alpha=1,label=None,save=False,titlefig="distrib"):
        '''
        
        --INPUT--
        time (scal, float64): time index for distrib plotting
        kde (bool, OPT)     : if True, a kernek density estimator is used to plot an empirical distribution    
        '''
        if ax==None:
            f,ax = plt.subplots(1,1,figsize=figsize)
        
        if label is None:
            label =  f"Histogram for {self.name} at time {time}"

        itime = [idx for idx,val in enumerate(self.time) if val==time][0]
        data  = np.real(self.traj[itime,:]) ## reuslts of AD analysis may result in complex numbers
        if kde:
            import seaborn as sns
            sns.kdeplot(data,ax=ax,color=color,alpha=alpha,label=label,fill=True)
        else:
            ax.hist(data,color=color,alpha=alpha,label=label,density=True)
        
        if save:    
            plt.savefig(f"titlefig_{self.name}_time{time}.png")
            
    def save(self,directory,name=None):
        """
        Save trajectory and associated time list as a dictionnary
        Dictionnary contains two keys: "time" with time indexes list and "var" with single or ensemble of trajectories
        
        --INPUT--
        name (str)      : name of saved variable. If None, self.name is used
        directory (str) : destination path for variable saving
        """
        if name is None:
            name = self.name
        
        dict_traj = {"time":self.time, "var":self.traj}
        full_path = os.path.join(directory,f"{name}.npy")
        
        np.save(full_path,dict_traj)
            
        
